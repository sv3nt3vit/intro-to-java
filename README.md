Homeworks and practice for my first programming class, Intro to Java back from 2013.

Most of the programs are trivial, except for some of the later assignments and a final project, which included an GUI application that can encrypt and decrypt a user entered text with a PIN login.

These excerpts were my first efforts in Java and programming overall, not including Pascal from school and an Informatics class with super old PC's running DOS :)