/**
 Program Name: DecryptionFilter
 Purpose:
 		This program decrypts the file
 		created in EncryprionFilter class;
 		restores the data, writes to a new file.
 Programmer: Ihar Laziuk
 Last Updated: 2014.05.06 
 */
import java.util.*;
import java.io.*;

public class DecryptionFilter {

	public static void main(String[] args) throws IOException {
		
		char [] convertedMessage=new char[100];   //create an array of char to hold uo to 100 values
		boolean endOfFile=false;	//EOF Flag
		
		//	open a SecretMessage.dat file for reading
      DataInputStream inputFile1 = new DataInputStream (new FileInputStream("SecretMessage.dat"));
      
      //open a file for writing and save decrypted content to a new file 
		DataInputStream inputFile2 = new DataInputStream (new FileInputStream("DecryptedSecretMessage.dat"));
      
      //open a DecryptedSecretMessage.dat for reading to display decrypted content
		DataOutputStream outputFile1 = new DataOutputStream (new FileOutputStream("DecryptedSecretMessage.dat"));
		
		System.out.print("\nYour decrypted message: ");
      
      //display encrypted message, use boolean endOfFile to exit the while loop
      while(!endOfFile){
			try{
				for(int i=0; i<convertedMessage.length; i++){
            
					convertedMessage [i] =inputFile1.readChar(); //read content from the encrypted file
					outputFile1.writeChar(convertedMessage [i]-10); //decrypt and write the content to a new file
					
               //read display decrypted content
               convertedMessage [i] =inputFile2.readChar(); 
					System.out.print(convertedMessage[i]);
				}
			} catch (EOFException e){
				endOfFile=true;
			}
		}
				
		inputFile1.close();
		outputFile1.close();				
	}
   
}
