/**
 Program Name: ExceptionsInPayrollExample
 Purpose:
	   		This program demonstrates how
	   		the Payroll class constructor
	   		throws custom exceptions.
Programmer: Ihar Laziuk
Last updated: 2014.05.06
*/
import java.util.*;
import java.io.*;

public class ExceptionsInPayrollExample {
	public static void main(String [] args){
		
      String name="";          // Employee name
   	int idNumber=0;         // ID number
   	double payRate=-1;       // Hourly pay rate
   	double hoursWorked=-1;   // Number of hours worked
      Scanner key=new Scanner(System.in);
     
     Payroll payroll=new Payroll("TT", 102030);
         
     while(name.equals("")){
        //force an NonValidNameException
   		try{
            System.out.println("Enter the name of account: ");        
            name=key.nextLine();
            payroll.setName(name);   
 			
   		} catch (NonValidNameException e) {
   			System.out.println(e.getMessage());
   		  }         
		}
      
      while(idNumber<=0){
        //force an InvalidIDValueException
   		try{      
            System.out.println("\nEnter the employee ID number: ");
            idNumber=key.nextInt(); 
            payroll.setIdNumber(idNumber);   
               			
   		} catch (InvalidIDValueException e) {
   			System.out.println(e.getMessage());
   		  }         
		}
      
       while(hoursWorked<0 || hoursWorked>84){
        //force an InvalidNumberOfHoursWorkedException
   		try{      
            System.out.println("\nEnter number of hours worked: ");
            hoursWorked=key.nextDouble(); 
            payroll.setHoursWorked(hoursWorked);   
               			
   		} catch (InvalidNumberOfHoursWorkedException e) {
   			System.out.println(e.getMessage());
   		  }         
		}
      
       while(payRate<0 || payRate>25){
        //force an InvalidPayRateException
   		try{      
            System.out.println("\nEnter pay rate: ");
            payRate=key.nextDouble(); 
            payroll.setPayRate(payRate);   
               			
   		} catch (InvalidPayRateException e) {
   			System.out.println(e.getMessage());
   		  }         
		}

      
      System.out.println("\n*********************************");
      System.out.println("Name of the account: "+payroll.getName());
      System.out.println("\nEmployee's ID number: "+payroll.getIdNumber());  
      System.out.println("\nHours worked: "+payroll.getHoursWorked()); 
      System.out.println("\nPay rate: "+payroll.getPayRate()); 
      System.out.println("*********************************");
      
	}

}
