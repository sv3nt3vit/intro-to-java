
/**
The NonValidNameException is thrown by the
Payroll class when an empty string for the
name parameter is passed to the constructor.
*/

public class NonValidNameException extends Exception{

	/**
	 This constructor uses a generic
	 error message
	 */
	public NonValidNameException() {
		super("Error: Empty string for the name passed\n");
	}
}


/**
The InvalidIDValueException is thrown by the
Payroll class when an invalid value is passed
for the employee's ID number in the constructor.
*/
class InvalidIDValueException extends Exception {
	/**
	 This constructor specifies the bad ID number
	 trying to pass in the error message
	 @param Invalid Employee's ID number
	 */
	
	public InvalidIDValueException(int idNumber) {
		super("Error: Invalid employee's ID number: "+idNumber+"\n");
	}
}


/**
The InvalidIDValueException is thrown by the
Payroll class when an invalid number is passed
for the number of hours worked e.g. a negative
number or a number>84 hours.
*/

class InvalidNumberOfHoursWorkedException extends Exception{
	
	/**
	 This constructor specifies the invalid
	 number of hours worked in the error message
	 @param Invalid number of hours worked.
	 */
	
	public InvalidNumberOfHoursWorkedException(double hoursWorked) {
		super("Error: Invalid number of hours worked: "+hoursWorked+"\n");
	}
}



/**
The InvalidPayRateException is thrown by the
Payroll class when an invalid number is passed
for the hourly pay rate; e.g. a negative
number or a number>25.
*/
class InvalidPayRateException extends Exception{
	/**
	 This constructor specifies the invalid 
	 hourly pay rate in the error message
	 @param Invalid pay rate.
	 */
	public InvalidPayRateException(double payRate) {
		super("Error: Invalid pay rate: "+payRate+"\n");
	}
}
