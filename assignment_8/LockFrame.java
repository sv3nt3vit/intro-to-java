/**
Program Name: LockFrame
Purpose: program displays 10 buttons.
		 In order to exit the right
		 combination must be clicked.
Default PIN: [693]
Developer: Ihar Laziuk
Last Updated: 2014.05.18
*/

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LockFrame extends JFrame {

	JButton button;
	private JPanel panel;
	private final int WINDOW_WIDTH = 280;
	private final int WINDOW_HEIGHT = 120;
	int cnt = 0;	//keeps track of digits in a combination 
	private String pin = "693";
	private StringBuilder pinCheck = new StringBuilder();
	
	
	//constructor creates and assembles the frame
	public LockFrame() {
		
		setTitle("Lock");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		createButtonPanel();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(panel);
		//pack();
		setVisible(true);
		//JOptionPane.showMessageDialog(null, "Enter the combination to exit");
	}

	
	// create GridLayout with 10 buttons
	public void createButtonPanel(){
		ButtonListener listener = new ButtonListener();
		panel = new JPanel(new GridLayout(2, 9));
		for(int i = 0; i <= 9; i++){
			button = new JButton(i + "");
			button.addActionListener(listener);
			panel.add(button);
		}
	}
	
	
	// implement response to buttons clicked; override actionPerformed() method  
	private class ButtonListener implements ActionListener  {		
		public void actionPerformed(ActionEvent event)  {
			
			/*
			  validate entered combination	
			 */
			pinCheck.append(event.getActionCommand());
			cnt++;	//keep track of pressed buttons in combination
			
			if(cnt == 3){	//combination reached 3 digits, validate
				if(pinCheck.toString().equals(pin)){
					JOptionPane.showMessageDialog(null, "Success!");
					System.exit(0);	//exit
				}else{	//invalid combination
					cnt = 0;	//reset cnt after invalid combination entered
					pinCheck.setLength(0);	//clear strBld object
					JOptionPane.showMessageDialog(null, "Wrong, try again");
				}	
			}			
		}	//end actionPerformed
	}	//end ButtonListener
	
	
	
	
	/**	MAIN METHOD creates an instance of the
	 	LockFrame class, displaying its window.
	 */
	public static void main(String[] args) {
		
		LockFrame lockFrame = new LockFrame();
	}

}	//end LockFrame class
