/**
Program Name: TestCounter
Purpose: program counts number
		 of words, chars and 
		 lines in TextArea
Developer: Ihar Laziuk
Last Updated: 2014.05.17
*/

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class TestCounter extends JFrame{
	
	//======== create fields (static variables)=========
	private JPanel panel;
      
	private JLabel numberOfWordsLabel;
	private JLabel numberOfCharsLabel;
	private JLabel numberOfLinesLabel;
	
	private JTextArea textArea;
	private JButton ProcessTextButton;
	private JButton clearAreaButton;      
	
	
	//create and assemble the frame
	public TestCounter() {
	      
		setTitle("Text Counter");
	    //setSize(300, 300);
	    createComponents();
	    setVisible(true);
	    setDefaultCloseOperation(EXIT_ON_CLOSE);	   
	
	}
	   
	
	//	create components JLabel, JTextArea, JButton and set Layout
	public void createComponents()   {
	    
		panel=new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
	    
	    //	create label "Number of Words" and set layout
	    numberOfWordsLabel = new JLabel("Number of Words: ");
	    JPanel numberOfWordsPanel = new JPanel();
	    numberOfWordsPanel.setLayout(new BorderLayout(0,0));
	    numberOfWordsPanel.add(numberOfWordsLabel, BorderLayout.CENTER);
	    
	    //	create label "Number of Chars" and set layout
	    numberOfCharsLabel = new JLabel("Number of Chars: ");
	    JPanel numberOfCharsPanel = new JPanel();
	    numberOfCharsPanel.setLayout(new BorderLayout(0,0));
	    numberOfCharsPanel.add(numberOfCharsLabel, BorderLayout.CENTER);
	    
	    //	create label "Number of Lines" and set layout
	    numberOfLinesLabel = new JLabel("Number of Lines: ");
	    JPanel numberOfLinesPanel = new JPanel();
	    numberOfLinesPanel.setLayout(new BorderLayout(0,0));
	    numberOfLinesPanel.add(numberOfLinesLabel, BorderLayout.CENTER);   
	    
	    /*	=== OR USE GRID LAYOUT AS ANOTHER OPTION TO ORGINIZE LABELS ===
	     * here create three panels, add labels 1 to 1 individually. Next
	     * create a panel to hold 3 other panels and add them to this panel. 
	      
	    JPanel allThreeInOnePanel = new JPanel();   
	    allThreeInOnePanel.setLayout(new GridLayout(3,1));
	    
	    allThreeInOnePanel.add(numberOfWordsPanel);
	    allThreeInOnePanel.add(numberOfCharsPanel);
	    allThreeInOnePanel.add(numberOfLinesPanel);
	    
	    panel.add(allThreeInOnePanel, BorderLayout.NORTH);
	    */
	    	    	 	    	    
	    //	add labels to panel
	    panel.add(numberOfWordsPanel);
	    panel.add(numberOfCharsPanel);
	    panel.add(numberOfLinesPanel);
	    
	    add(panel);
	    
	    createTextArea();	//call method to create TextArea
	      
	    createButtonPanel(); 	//call method to create buttons
	        
	    pack();  //take care of layout
	}
	   
	
	
	//create and assemble the frame
	public void createTextArea() {
	   
		textArea = new JTextArea(5, 25);
	    //textArea.setLineWrap(true);
	    textArea.setWrapStyleWord(true);
	      
	    JScrollPane scrollPane = new JScrollPane(textArea);
	      
	    JPanel textAreaPanel = new JPanel();
	    
	    textAreaPanel.add(scrollPane);
	    panel.add(textAreaPanel, BorderLayout.CENTER);
	}
	   
	
	
	//create and assemble buttons on panel, register to ActionListener
	public void createButtonPanel() {
	   
		ButtonListener listener = new ButtonListener();
	     
	    ProcessTextButton = new JButton("Process Text");
	    ProcessTextButton.addActionListener(listener);
	    
	    clearAreaButton = new JButton("Clear Area");
	    clearAreaButton.addActionListener(listener);
	      	     	      
	    JPanel buttonPanel = new JPanel();
	    buttonPanel.add(ProcessTextButton);
	    buttonPanel.add(clearAreaButton);
	      
	    panel.add(buttonPanel, BorderLayout.SOUTH);	     
	}


	      
	   
	// implement response to buttons clicked; override actionPerformed() method  
	private class ButtonListener implements ActionListener  {
		
		public void actionPerformed(ActionEvent event)  {
			String text;
			int numOfChars, numOfWords = 0, numOfLines = 0; //var-s to hold count values
			
	        //	"Process Text" button pressed; calculate and display
			//number of chars, words and lines in text area
			if(event.getSource()==ProcessTextButton){
	        	
	        	text = textArea.getText(); //access entered Text from JTextArea
	        	
	        	// ========	count number of CHARS including empty spaces =======
	        	numOfChars = text.length();
	        	numberOfCharsLabel.setText("Number of Chars: " + numOfChars);	//display count
	        	
	        	
	        	// =========== count number of WORDS ==============
	        	StringTokenizer tokens = new StringTokenizer(text, " !@#$%^&*()_+=,.<>?/|\\~1234567890'\"-\n\t");
	        	
	        	//	ANOTHER OPTION instead StringTokenizer object, NOT USED HERE
	        	//String [] tokens = text.split("[ !@#$%^&*()_+=,.<>?/|\~1234567890-]");
	        		        	
	        	//as long there more delimiters count words
	        	while(tokens.hasMoreTokens()){
	        		tokens.nextToken();
	        		numOfWords++;
	        	}
	        	
	        	
	        	if(numOfChars !=0){ //user entered at least one char
	        		numberOfWordsLabel.setText("Number of Words: " + numOfWords);	//display count
	        	} else {	//user has not entered anything
	        		numberOfWordsLabel.setText("Number of Words: " + 0);	//display count
	        	}
	        	
	        	
	        	//	========== count number of LINES ===========
	        	//may use method getWrappedLines(), need to find out how it works exactly	        	
	        	if(numOfChars !=0){
	        		numOfLines = textArea.getLineCount();
	        		numberOfLinesLabel.setText("Number of Lines: " + numOfLines);	//display count
	        	} else {
	        		numberOfLinesLabel.setText("Number of Lines: " + 0);	//display count
	        	}
	     
	        }  
	        
	        
	        //	"Clear Area" button clicked, respond and clear text area
	        if(event.getSource() == clearAreaButton){
		        textArea.setText("");
	        }
	        	        
	    } //end actionPerformed      
	}	//end ButtonListener
	   
		        
	
	
	
	//	MAIN METHOD to run the program
	public static void main(String[] args) {
		
		TestCounter testCounter = new TestCounter();		
	}

}	//	end TextCounter class
