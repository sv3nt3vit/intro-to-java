import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class BorderTest extends JFrame 
{
   private JPanel panel;
   private JRadioButton redButton;
    private JRadioButton greenButton;


   
   public BorderTest()
   {
      setTitle("Border Frame");
      
      panel=new JPanel();
      //setSize(300, 200);
      //StyleOptionsFrame styleOptions=new StyleOptionsFrame();
      RadioButtonFrame   colorOptions=new RadioButtonFrame();    
      
      
      //JPanel stylePanel=styleOptions.createComponents();
      //stylePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "styles"));
      
      JPanel colorPanel=colorOptions.createComponents();

      //panel.add(stylePanel);
      panel.add(colorPanel);

      add(panel);
      pack();
      setVisible(true);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
     
   }
   
   public void createComponents()
   {
    
    }
   
        
   //inner class : a class inside another class
   
   private class radioButtonListener implements ItemListener
   {
      public void itemStateChanged(ItemEvent evt)
      {
         
                               
      }

   
   }
   
   
   public static void main(String[] args)
   {
      
       BorderTest border=new BorderTest(); 
   
   }
 }
   
   

