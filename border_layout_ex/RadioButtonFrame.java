import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RadioButtonFrame extends JFrame 
{
   private JPanel panel;
   private JRadioButton lineButton;
   private JRadioButton etchedButton;
   private JRadioButton raisedBevelButton;
   private JRadioButton loweredBevelButton;
   private JRadioButton matteButton;
   private JRadioButton titledButton;

   
   public JPanel createComponents()
   {
    
      panel=new JPanel();
      panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
      
     // panel.setBorder(BorderFactory.createLineBorder(Color.RED, 10));
      //panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED, 10), "colors"));

   panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "border test"));
      createRadioButtons();     
                        
      add(panel);
      
      return panel;
      
   }
   
    public void createRadioButtons()
  {
      RadioButtonListener listener=new RadioButtonListener();
      
      lineButton=new JRadioButton("line");
      lineButton.setMnemonic(KeyEvent.VK_R);
      lineButton.setToolTipText("Change the color to red.");    
      lineButton.addItemListener(listener);
      
      
      etchedButton=new JRadioButton("etched");
      etchedButton.setMnemonic(KeyEvent.VK_G);
      etchedButton.setToolTipText("Change the color to green.");
      etchedButton.addItemListener(listener);
      
      
      raisedBevelButton=new JRadioButton("raised bevel");
      raisedBevelButton.setMnemonic(KeyEvent.VK_G);
      raisedBevelButton.setToolTipText("Change the color to green.");
      raisedBevelButton.addItemListener(listener);
    
      
      loweredBevelButton=new JRadioButton("lowered bevel");
      loweredBevelButton.setMnemonic(KeyEvent.VK_G);
      loweredBevelButton.setToolTipText("Change the color to green.");
      loweredBevelButton.addItemListener(listener);
      
      
      matteButton=new JRadioButton("matte");
      matteButton.setMnemonic(KeyEvent.VK_G);
      matteButton.setToolTipText("Change the color to green.");
      matteButton.addItemListener(listener);
      
      
      titledButton=new JRadioButton("titled");
      titledButton.setMnemonic(KeyEvent.VK_G);
      titledButton.setToolTipText("Change the color to green.");
      titledButton.addItemListener(listener);
     
      ButtonGroup group=new ButtonGroup();
      group.add(lineButton);
      group.add(etchedButton);
      group.add(raisedBevelButton);
      group.add(loweredBevelButton);
      group.add(matteButton);
      group.add(titledButton);
      
      
     
     /*boldBox=new JCheckBox("Bold");
     boldBox.addItemListener(listener);

     exitBox=new JCheckBox("Exit");
     exitBox.addItemListener(listener);
      */
      panel.add(lineButton);
      panel.add(etchedButton);
      panel.add(raisedBevelButton);
      panel.add(loweredBevelButton);
      panel.add(matteButton);
      panel.add(titledButton);
    
  }
    
    
   //inner class : a class inside another class
   
  private class RadioButtonListener implements ItemListener
   {
      public void itemStateChanged(ItemEvent evt)
      {
         if(lineButton.isSelected()) {
            panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
         }  
         
         
         else if(etchedButton.isSelected()){
            panel.setBorder(BorderFactory.createEtchedBorder());           
         }  
         
         else if(raisedBevelButton.isSelected()) {
            panel.setBorder(BorderFactory.createRaisedBevelBorder());          
         }  
         
         else if(loweredBevelButton.isSelected()) {
            panel.setBorder(BorderFactory.createLoweredBevelBorder());          
         }  
         
         else if(matteButton.isSelected()) {
            panel.setBorder(BorderFactory.createMatteBorder(1,5,1,1, Color.RED));          
         }  
         
        /* else if(titledButton.isSelected()) {
            panel.setBorder(BorderFactory.createMatteBorder(centered));          
         }  
        */                       
      }

   
   }
 
   
   public static void main(String[] args)
   {
      
       RadioButtonFrame frame=new RadioButtonFrame(); 
   
   }
 }
   
   

