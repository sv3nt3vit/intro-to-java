import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Calendar;

public class StyleOptionsFrame extends JFrame 
{
   private JPanel panel;
   private JTextField textField;
   private JCheckBox italicBox;
   private JCheckBox boldBox;
   private JCheckBox exitBox;


   
  /* public StyleOptionsFrame()
   {
      setTitle("StyleOption Frame");
      //setSize(300, 200);
      
      createComponents();
      pack();
      setVisible(true);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
     
   }*/
   
   public JPanel createComponents()
   {
    
      panel=new JPanel();
      panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
      
      createLabel();
      createTextField();
      createCheckBoxes();     
                        
      add(panel);
      
      return panel;
      
   }
   
   public void createLabel()
   {
   
      JLabel label=new JLabel("Style Options");
      
      JPanel labelPanel=new JPanel();
      
      labelPanel.add(label);
      panel.add(labelPanel);
      
      
   }
   
   public void createTextField()
   {
   
    JPanel textFieldPanel=new JPanel();
    textField=new JTextField(10);
    
    textFieldPanel.add(textField);
    panel.add(textFieldPanel);

    
    
      

   
   }

   
  public void createCheckBoxes()
  {
     checkBoxListener listener=new checkBoxListener();
      
     italicBox=new JCheckBox("Italic");
    
     italicBox.setMnemonic(KeyEvent.VK_I);
     italicBox.setToolTipText("Change to italic style.");
     
     italicBox.addItemListener(listener);
     
     boldBox=new JCheckBox("Bold");
     boldBox.addItemListener(listener);

     exitBox=new JCheckBox("Exit");
     exitBox.addItemListener(listener);

    JPanel boxPanel=new JPanel();
    
    boxPanel.add(italicBox);
    boxPanel.add(boldBox);
    boxPanel.add(exitBox);

    panel.add(boxPanel);
    
  }
    
    
   //inner class : a class inside another class
   
   private class checkBoxListener implements ItemListener
   {
      public void itemStateChanged(ItemEvent evt)
      {
      
          int style=Font.PLAIN;
          
          if(italicBox.isSelected())
          {
            style+=Font.ITALIC;
            //System.out.println("italic");
          }
          
          if(boldBox.isSelected())
          {
            style+=Font.BOLD;
            //System.out.println("bold");
          }
          if(exitBox.isSelected())
            System.exit(0);
           
           Font font=textField.getFont();
           Font f=new Font(font.getName(), style ,font.getSize());
           
           textField.setFont(f);
   
                               
      }

   
   }
   
   
   public static void main(String[] args)
   {
      
       StyleOptionsFrame frame=new StyleOptionsFrame(); 
   
   }
 }
   
   

