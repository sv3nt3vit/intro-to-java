public class AcctTest
{
   public static void main(String[] args)    
   {
           
      try
      {
            CheckingAccount acct=new CheckingAccount("Mike", -300);
            
            acct.checkBalance(); 
            acct.withdraw(100);
            
            System.out.println("balance: "+acct.getBalance());
      
            
            acct.withdraw(500);
             
            System.out.println("balance: "+acct.getBalance());
            
                                 
       }     
             
       catch(InsufficientFundException e)
       {
         System.out.println(e.getMessage());
         System.out.println("short: "+ e.getAmount());  
       }  
      
       catch (NegativeBalanceException e){
            System.out.println(e.getMessage());
            System.out.println("\nYour account is down by "+e.getAmount());
       }
   }  
}