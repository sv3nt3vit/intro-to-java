public class CheckingAccount
{
   private String acctNum;
   private double balance;
   
   public CheckingAccount(String acctNum, double balance)
   {
      this.acctNum=acctNum;
      this.balance=balance;
   }
   
   public void deposit(double amt)
   {
      balance+=amt;
   }
   
   public void withdraw(double amt) throws InsufficientFundException
   {
      if(amt>balance)
        throw new InsufficientFundException(amt-balance);   
      else
         balance-=amt;
   
   }
   
   public double getBalance()
   {
      return balance;
   }
   
   public void checkBalance() throws NegativeBalanceException{
      if(balance<0)
         throw new NegativeBalanceException();
      else
         System.out.println("Yuor balance is up by "+balance+" $");
   }
   
}
   