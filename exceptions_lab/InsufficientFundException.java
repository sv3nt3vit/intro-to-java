public class InsufficientFundException extends Exception
{
   private double amount;
   
   public InsufficientFundException()
   {
      super("Insufficent fund.");
     
   }
   
   public InsufficientFundException(double amt)
   {
        super("Insufficent fund.");
        amount=amt;
     
   }
   
   public double getAmount()
   {
      return amount;
   }
}

   