import java.util.*;
public class Mailbox{
   
   private ArrayList<Message>emails=new ArrayList<>();
   private Message msg;
   
   public void addMessage(Message m){
      emails.add(m); 
   }
   
   public Message getMessage(int i){
      return emails.get(i);
   }
   
   public void deleteMessage(int i){
      emails.remove(i);
   }

}