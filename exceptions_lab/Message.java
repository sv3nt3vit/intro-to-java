import java.util.*;
public class Message{

private String sender, recipient, messageText;

   public Message(String sender, String recipient){
      this.sender=sender;
      this.recipient=recipient;
   }
   public Message (String sender, String recipient, String t){
      this(sender, recipient);
      messageText=t;
   }

   public Message(Message m){
      sender=m.sender;
      recipient=m.recipient;
      messageText=m.messageText;   
   }
   
   public void append(String line){
      messageText+=line; 
   }
   
   public boolean equals(Message m){
   
   //String messageText2=new String(m);
      if(messageText.equals(m.messageText)&&sender.equals(m.sender)&&recipient.equals(m.recipient))
         return true;
      else
         return false;
   } 
   
   public String toString(){
      return "From: "+sender+"\nTo"+recipient+"\n"+messageText;
   }   
}