public class NegativeBalanceException extends Exception
{
   private double amount;
   
   public NegativeBalanceException()
   {
      super("In your account accured negative balance.");
     
   }
   
   /*
   public NegativeBalanceExceptions(double amt)
   {
        super("In your account accured negative balance.");
        amount=amt;  
   }
   */
   
   
   public double getAmount()
   {
      return amount;
   }
}

   


