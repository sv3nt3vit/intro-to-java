import javax.swing.*;
import java.awt.*; //abstract window toolkit
import java.awt.event.*;

public class MyNewFrame extends JFrame implements ActionListener
{
    private JPanel panel;
    JButton button;
    int counter=0;
    
    public MyNewFrame()
    {
       setTitle("Super Frame");
       setSize(300, 400);
       
       panel=new JPanel(); //use the panel as a container to hold the components
       panel.setBackground(Color.GREEN);
     
       button=new JButton("0");
       button.addActionListener(this);
       button.setText(counter+"");

      
       panel.add(button);
       
       add(panel);
       
       setVisible(true);
       setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
   
   public void actionPerformed(ActionEvent event){
      //JOptionPane.showMessageDialog(null, "Hello there!");    
       Color currentColor=panel.getBackground();     
      //change background color
      if(currentColor==Color.GREEN){    
        panel.setBackground(Color.RED);
        counter++;
        button.setText(counter+"");
        setSize(300, 400);

      }
      else{
         counter++;
         button.setText(counter+"");
     
         panel.setBackground(Color.GREEN);
         counter++;
         button.setText(counter+"");
      }
   }
   
   public static void main(String[] args)
   {      
        new MyNewFrame();  
   }
}