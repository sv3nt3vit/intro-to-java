import java.io.*;

public class ObjectOutput
{
   public static void main(String[] args) throws IOException
   {
      Student[] students={new Student("Mike", 3.95), new Student("Sarah", 3.57), new Student("Alex", 3.2)};
      
      FileOutputStream fstream=new FileOutputStream("student.dat");
      ObjectOutputStream out=new  ObjectOutputStream (fstream);
      
      for(int i=0; i<students.length; i++)
      {
         out.writeObject(students[i]);
      }
      
      out.close();
      
    }
}