import java.io.*;

public class Student 
{
   private String name;
   private double GPA;
   
   public Student(String n, double gpa)
   {
      name=n;
      GPA=gpa;
   }
   
   public String getName()
   {
   
      return name;
   }
   
   public double getGPA()
   {
      return GPA;
   }
}
   