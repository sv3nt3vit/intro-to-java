public class Area{
   
   public static double getArea(double r){//circle's area
   
      return Math.PI*Math.pow(r, 2);
   }
   
   public static double getArea(double w, double l){//rectangles's area
   
      return w*l;
   }
   
   public static double getArea (double w, double l, double h){//cube's area
   
      return w*l*h;
   }
}