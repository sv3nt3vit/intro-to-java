/* 
Write an algorithm to ask the user for the length and width of a rectangular room,
and then display the room�s area. 

Area of a room

1. Display "What is the length of the room?"
2. The user inputs the length and store it to the memory.
3. Display "What is the width of the room?"
4. The user inputs the width and store it to the memory.
5. Calculate the area by multiplying width and length and store it to the memory
6. Display the area.


*/

// /* */ is a comment symbol, which comments a block of text. 

// The content will be ignored by the compiler.


//syntax errors: A syntax error is an error that introduced by the programmer which voilates the 
// rules of the programming language.

//Where there are no synatx errors, the program can pass the compilation. And it will generate
// a new class file. For AreaOfARoom.java, AreaOfARoom.class will be created. Only if there is
// a class file, the program can run.

// .java file is called the source code.
// .class file is called the class file, which is not readable for us.

/* The elements of programs:

1. statements: instructions for computers to follow to perform a task.
	       Each statement ends with a semi-colon.
   
   statements vs. lines:  A statement can have multiple lines.
			  A line can have multiple statements. However, it is not a good practice
			  to put multiple statements in one line.  

2. keywords: Keywords have special meanings in the program. For example, public, class, static, 
	     void, and int in this program. int means data type for integers. The keywords are
	     resevered for java programming language. The programmers cannot use them for other
	     purposes. public means it can be accessed from outside of the program.

3. variables: Variables are labels of memory locations. Variable are changable. For example, length,
	      width, and area in this program.

4. punctuations: ; " " .

5. operators: *, = (assignment operator) 
*/



import java.util.*;

public class AreaOfARoom 
{ 
   //

     // the main method is the entry point of the program
     public static void main(String[] args)
     {
	//1. Display "What is the length of the room?"
	System.out.println("What is the length of the room?");
	
	
	//2. The user inputs the length and store it to the memory.
	Scanner keyboard=new Scanner(System.in);
	
	//allocate memory for storing the input
	// int is a data type for integerd indicating 4 bytes of memory is needed.
	//length is a variable name, which is a label of the memory allocated
	int length=keyboard.nextInt();	//read in an integer from keyboard
	
	
	//3. Display "What is the width of the room?"
	System.out.println("What is the width of the room?");
	

	//4. The user inputs the width and store it to the memory.
	int width=keyboard.nextInt();

	//5. Calculate the area by multiplying width and length and store it to the memory
	
	 int area=length*width; // 'x' is not multiplication operator
				// z=xy is not allowed in the program for mutilplication

	//6. Display the area.	
	System.out.println("The area is "
			  +area);
	

	// about = assignment operator

	// read it as assign 14 to num1. Don't read it as "num1 equals to 14"
        // num1 is a memory location. 14 is a value.
        int num1=14;
	int num2=90;
        

        int num3=num1+num2;
        //int num3=14+19;

	System.out.println("num3="+num3);


     }

   //
}