import java.util.*;

public class ArmstrongNumber{
   public static void main(String [] args){
   
      int cnt=0;
      
      for (int i=1; i<101; i++){
         Random rand=new Random();
         int num=rand.nextInt((999-100)+1)+100;

          if (isArmstrongNum(num)){
            cnt++;
            System.out.print(num+" ");        
          }
          else if(i==100&&cnt<1)
            System.out.println("No Armstrong numbers found among "+i+" generated digits\n\t\tTry again"); 
          else
            ;
      }
      
   }
   
  public static boolean isArmstrongNum(int num){
   
      int n=num;
      int sum=0;
      
      //starting with the last digit of the rand number, 
      //raise that num to the pow of 3 and add to sum.
      //use modulus 10 and division by 10 in a loop to switch
      //to the next digit within the number untill (int)num/10=0 
      while(num !=0){
         int rem=num%10;
         sum+=Math.pow(rem, 3);
         num=num/10;
      }
      
      if (n==sum)
         return true;
      else
         return false; 
         
   }
}
   
   
   