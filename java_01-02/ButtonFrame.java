import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ButtonFrame extends JFrame{
  
   private JPanel panel;
   private JButton redButton;
   private JButton greenButton;
   private JButton blueButton;
   private JButton grayButton;
   private int img_height=60;
   private int img_width=60;
  
   public ButtonFrame(){
      setTitle("Magic Button");
      setSize(300, 200);
     
     
      createComponents();
      setVisible(true);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
   }
  
   public void createComponents(){
  
      panel=new JPanel(); 
     
        //to add image
      ImageIcon icon=new ImageIcon("per.png");
      
      //to resize the image use 3 following lines of code, plus declare and initialize height and width;
      Image img = icon.getImage() ;  
      Image newimg = img.getScaledInstance( img_height, img_width,  java.awt.Image.SCALE_SMOOTH ) ;  
      icon = new ImageIcon( newimg );
      
      ButtonListener listener=new ButtonListener();

      redButton=new JButton("Red", icon);     
      redButton.addActionListener(listener);  //register the listener to the button
     
      greenButton=new JButton("Green", icon);
      greenButton.addActionListener(listener);
      
      blueButton=new JButton("Blue", icon);
      blueButton.addActionListener(listener);
      
      grayButton=new JButton("Gray", icon);
      grayButton.addActionListener(listener);



     
      //another way of doing the prevous 3 lines of code, to reduce the code is
      //button.addActionListener(new ButtonListener());
     
      panel.add(redButton);
      panel.add(greenButton);
      panel.add(blueButton);
      panel.add(grayButton);
     
      add(panel); //add panel to the frame
  
   }
  
   //inner class: a class inside another class
  
   private class ButtonListener implements ActionListener{
     
      public void actionPerformed(ActionEvent event){//register the listener for this button
     
      //use evt.getSource() returns the components generated event
      if(event.getSource()==redButton)
         panel.setBackground(Color.RED);
      else if (event.getSource()==greenButton)
         panel.setBackground(Color.GREEN);
      else if (event.getSource()==blueButton)
         panel.setBackground(Color.BLUE);
      else
         panel.setBackground(Color.GRAY);         
      }
   }
  
   public static void main(String [] agrs){
  
      ButtonFrame frame=new ButtonFrame();
         
   }  
}