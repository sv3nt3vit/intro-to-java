import java.util.*;

public class CalcArea{
   public static void main(String []args){
   
   double r, width, length, height;
   
      Scanner input=new Scanner(System.in);
      System.out.println("Enter radius");  
      r=input.nextDouble();
      
      System.out.println("Enter width");
      width=input.nextDouble();
      
      System.out.println("Enter length");
      length=input.nextDouble();
      
      System.out.println("Enter height");
      height=input.nextDouble();
      
      System.out.println("***************************************************");
      System.out.println("The area of the circle\t =\t"+Area.getArea(r));
      System.out.println("The area of the circle\t =\t"+Area.getArea(width,length));
      System.out.println("The area of the circle\t =\t"+Area.getArea(width,length,height));
      System.out.println("***************************************************");
   }   
} 