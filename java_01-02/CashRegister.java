/*
Class Name: CashRegister
Purpose: to be used with
class RetailItem
Programmer: Ihar Laziuk
Last Updated: 2014.31.03
*/

public class CashRegister{
   
   private int numOfItems;
   private RetailItem item;
   private final double TAX=0.6;
   private int subtotal;
   private double price;
      
   //constructor to initialize
   public CashRegister(RetailItem unit, int quant){
      item=unit;
      numOfItems=quant;   
   }
   
   //getSubtotal method
   public double getSubtotal(){
      return item.getPrice()*numOfItems;
   }
   
   //getTax method
   public double getTax(){
      return getSubtotal()*TAX;
   }   
   
   //getTotal method
   public double getTotal(){
      return getSubtotal()+getTax();
   }
    
    CashRegister register=new CashRegister(item, 1);
    
    public void printReceipt(){
    System.out.println("SALES RECEIPT\n"+
                       "Unit Price: "+item.getPrice()+
                       "\nQuantity: "+item.getQuant()+
                       "\nSubtotal: "+register.getSubtotal()+
                       "\nSales Tax: "+register.getTax()+
                       "\nTotal: "+register.getTotal());  
    }
 
}