/**
Program name: Century
Purpose: calculates
         user's age
Programmer: Ihar Laziuk
Date: 10.12.2013
*/

import java.util.*;

public class Century
{
   public static void main(String [] args)
   {
      double age;
      
      //prompt the user to enter the year of birth and the current year
      System.out.println("Enter 2 last digits of your year of birth");
      Scanner keyboard=new Scanner(System.in);
      int birthYear= keyboard.nextInt();
      
      System.out.println("Enter the current year");
      int currentYear= keyboard.nextInt();
      
      
      //calculate user's age
      
      if (currentYear-birthYear>0)
         age=currentYear-birthYear;
      else {
         currentYear=2000+currentYear;
         birthYear=1900+birthYear;
         age=currentYear-birthYear;
         }
         
         
         //display the user's age
         System.out.println("Year of Birth: "+birthYear+
                            "\nCurrent year is: "+currentYear+
                            "\nYour age is: "+age);
     }
}

         
      
      
     
      