

public class CheckingAccount{

   private int acctNo;
   private String name;
   private double balance;
   private boolean status;
   private double minBal;
   private final double SERVICE_CHARGE;
   
   public CheckingAccount(){
      
      acctNo=988015602;
      name="Campus Edge Checking";
      balance=0;
      status=false;
      minBal=25.0;
      SERVICE_CHARGE=11.50;
   }
   
   public void numOfAcct(int givenNum){
      
      acctNo=givenNum;
   }
  
   public void setName(String givenName){
   
      name=givenName;
   }
   
   public boolean isInactive(){
   
      if(balance<minBal){
         System.out.println("Your balance fell below minimum balance\n\t\tYour account is Inactive");
         return true;
      }
      else
         return false;
   }
   
   public void setBalance(double newBal){

      balance=newBal;
   }
   
   public void withdraw(double amount){
      
      if(!isInactive()&&balance>amount)   
         balance-=amount;
      else
         System.out.println("Your account is inactve\nNo withdrawals can be made");
   }
   
   public void displayBalance(){
   
      System.out.println("Your current balance is: $"+balance);
   }
   
    public double getBalance(){
   
      return balance;
   }

   
   public void deposit(double amount){
      
      if(isInactive()&&amount+balance>minBal){
      
         balance+=amount;
      }
      else if(amount+balance<minBal){
         balance+=amount;
         System.out.println("Your account is still inactive");
      }         
       else
         balance+=amount;
   }
   
   public void getMonthlyStatement(){
      
      System.out.println("=============================================");
      System.out.println("\tYour account summary:");
      System.out.println("Account name: \t\t"+name+"\nAccount number: \t"+acctNo);
      
      if(balance<minBal){
         System.out.println("The current balance is below minimum balance");
         System.out.println("Service Fee: $"+SERVICE_CHARGE+"\nYour balance: $"+(balance-SERVICE_CHARGE));
      }
      else{
         System.out.println("Your balance: \t\t$"+balance);
      }
      System.out.println("=============================================");
   }   
}   