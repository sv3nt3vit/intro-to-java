import java.io.*;
import java.util.*;

public class CheckingAccountTest{
   
   public static void main(String [] args)throws IOException {
   
      CheckingAccount myAccount=new CheckingAccount();
          
      myAccount.setBalance(100);
      double balance=myAccount.getBalance();
      
      
      File file=new File("deposit.txt");
      Scanner inputFile=new Scanner(file);
      
      while(inputFile.hasNextDouble()){
         double number=inputFile.nextDouble();
         balance+=number;  
      }
  
      inputFile.close();
      myAccount.setBalance(balance);
      
      File file2=new File("withdraw.txt");
      Scanner inputFile2=new Scanner(file2);
      
      while(inputFile2.hasNextDouble()){
         double number=inputFile2.nextDouble();
         balance-=number;
      }
      
      inputFile2.close();

      myAccount.setBalance(balance);
      
      myAccount.getMonthlyStatement();
    
   }
}