/**************************
Program Name: CoinTossing
Purpose: simulate realistic
         coin tossing
Programmer: Ihar Laziuk
Date: 11.28.2013
***************************/

import java.util.*;
public class CoinTossing {
   
   
      //create a boolean method to simulate coin tossing
   public static boolean flip(){
      
      /**
      flip a boolean method for coin tossing
      @return false for tails
      @return true for heads
      */
   
      Random rand=new Random();// create a random class Random
      int num=rand.nextInt(2)+1;//define the range 1-2; whereas 1 - heads, 2 - tails
      
      int heads=1;
      int tails=2;
   
      if (num==heads)//num=heads(1) 
         return true;
      else//num=tails(2) 
         return false;
   } 
       

   public static void main(String [] args){
   
      Scanner keyboard=new Scanner(System.in);
      int headsCount=0, tailsCount=0;
      System.out.println("Would you like to Toss Coin?\n\t--Enter Y/N--");
      String str=(keyboard.nextLine()).toUpperCase();//read the answer and convert to upper case
      char ans=str.charAt(0);
      
      while (ans=='Y'){//as long as user prompts simulate coin tossing counting each return of heads and tails
         flip();
         if(flip()==true){//true=heads(1)
            headsCount++;
            System.out.println("Current HEADS count: "+headsCount);}
         else{//flip()==false - tails(2)
            tailsCount++;
            System.out.println("Current TAILS count: "+tailsCount);}
            
         System.out.println("Would you like to Toss Coin?\n\t--Enter Y/N--");
         str=(keyboard.nextLine()).toUpperCase();//read the answer and convert to upper case
         ans=str.charAt(0);
      }
         //When user chooses NOT to Toss Coin anymore - display FINAL statistics for count of both heads and tails
      System.out.println("\nFinal satitistics:\n");
      System.out.println("*******************");
      System.out.println("Count of HEADS = "+headsCount);
      System.out.println("***             ***");
      System.out.println("Count of TAILS = "+tailsCount);
      System.out.println("*******************\n");
         
         //determine and display the winner
      if(headsCount>tailsCount)
         System.out.print("^^^HEADS WON^^^");
      else if(tailsCount>headsCount)
         System.out.println("^^^TAILS WON^^^");
      else
         System.out.println("^^^It is a DRAW^^^");
   
      System.exit(0);
   }
}