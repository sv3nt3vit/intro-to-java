/****************************************
Program name: ConverterToCelsius
Purpose: Convertion Fahrenheit to Celsius
Programmer: Ihar Laziuk
Date of creation: 09.25.2013
Last update: 10.03.2013
*****************************************/

import java.util.*;
public class ConverterToCelsius
{
   public static void main(String [] args)
   {  
      //1.Display "What is the temperature in Fahrenheit?"
      System.out.println("What is the temperature in Fahrenheit?");
      
      //2.Let the user input the temperature in Fahrenheit and store it to the memory
      Scanner keyboard=new Scanner(System.in);
      double tempFahr=keyboard.nextDouble();
      
      //3.Perform convertion to Celsius
      double tempCels=(tempFahr-32)*(5.0/9);
      
      //4.Display temperature in Celsius
      System.out.println("The temperature in Celsius is "+tempCels+ "'C.");
      }     
}
      