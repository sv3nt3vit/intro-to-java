/****************************************
Program name: ConverterToFahrenheit
Purpose: Convertion Celsius to Fahrenheit
Programmer: Ihar Laziuk
Date of creation: 09.25.2013
Last update: 10.03.2013
*****************************************/

import java.util.*;
public class ConverterToFahrenheit
{
   public static void main(String [] args)
   {  
      //1.Display "What is the temperature in Celsius?"
      System.out.println("What is the temperature in Celsius?");
      
      //2.Let the user input the temperature in Celsius and store it to the memory
      Scanner keyboard=new Scanner(System.in);
      double tempCels=keyboard.nextDouble();
      
      //3.Perform convertion to Fahrenheit
      double tempFahr=(tempCels*(9.0/5))+32;
      
      //4.Display temperature in Fahrenheit
      System.out.println("The temperature in Fahrenhiet is "+tempFahr+ "'F.");
      }     
}
