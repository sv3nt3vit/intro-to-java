/*********************************
* Algorithm name: AvailableCredit
* Purpose: calculates customer's
  available credit
* Programmer: Ihar Laziuk
* Date created: 09.06.2013
* Last update: 11:55 09.14.2013
*********************************

1. Display "What is your maximum amount of credit?".
2. Let the user input maximum amount of credit and store it to the memory.
3. Display "What is the amount of credit used?".
4. Let the user input amount of credit used and store it to the memory.
5. Calculate available credit by subtracting the amount of credit used from the
   maximum amount of credit.  
6. Display "Your available credit is $".
*/

import java.util.*; // to get input from the keyboard

public class CurrentBalance
{
   public static void main(String[] args)
   {
   //1.Display "What is the starting balance?".
   System.out.println("What is the starting balance?");
   
   //2.Let the user input the starting balance and store it to the memory.
   Scanner keyboard=new Scanner(System.in);
   double startingBalance=keyboard.nextDouble();
   
   //3. Display "What is the total dollar amount of deposits made?".
   System.out.println("What is the total dollar amount of deposits made?");
   
   //4. Let the user input the total dollar amount of deposits made and store it to the memory.2,5
   double depositsMade=keyboard.nextDouble();
   
   //5.Display "What is the total dollar amount of withdrowals made?"
   System.out.println("What is the total dollar amount of withdrowals made?");
   
   //6.Let the user input the total dollar amount of withdrowals made and store it to the memory.
   double withdrowalsMade=keyboard.nextDouble();
   
   //7. Display "What is the monthly interest rate?".
   System.out.println("What is the monthly interest rate?");
   
   //8.Let the user input the monthly interest rate (for ex for 2% input 2, for 2.5% - 2,5) and store it to the memory.
   double monthlyInterestRate=keyboard.nextDouble();
   
   //9.Calculate current balance by adding earned interest to (startingBalance+depositsMade-withdrowalsMade)
   //whereas earned interest can be calculated by 
   //(startingBalance+depositsMade-withdrowalsMade)* monthly interest rate
    
   double CurrentBalance=(startingBalance+depositsMade-withdrowalsMade)+(startingBalance+depositsMade-withdrowalsMade)*(monthlyInterestRate/100);
   
   //10. Display " Your current balance is $".
   System.out.println("Yout current balance is $"+CurrentBalance);
   
   }
}
