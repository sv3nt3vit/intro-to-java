/*****************************
Program's name: DieTossing
Program's purpose: simulate
die tossing in an array, mark
runs and print die values

Programmer: Ihar Laziuk
Date: 02.16.2014
Last updated:02.22.2014
******************************/

import java.util.*;

public class DieTossing {
   public static void main (String []args) {
      
      Random rand=new Random();//create a random class
      
      int[]diceThrows=new int[20]; //create an array size 20
      
      boolean inRun=false;
      
      for(int i=0; i<diceThrows.length; i++){
         diceThrows[i]=rand.nextInt(6)+1;//specify the range of possible numbers in the array
      }
  
      for(int i=0; i<diceThrows.length; i++)
      	
      {   
         if (inRun){  //if the current value does not equal the previous value,
         			// close parenthesis for previous numbers in run
            if(diceThrows[i] != diceThrows[i-1]) { 
                System.out.print(")");   
                inRun=false;
            }
          }
          
         if (!inRun){ //according to the alg print open parenthesis when current value the same as following 
             if (i < diceThrows.length -1 && diceThrows[i] == diceThrows[i+1] ) {  
                  System.out.print("(");
                  inRun=true;
             }                     
         }       
        
         System.out.print(diceThrows[i]+" ");//printing values of the array     
         
      }
      
      if(inRun)
         System.out.print(")");
   }
}