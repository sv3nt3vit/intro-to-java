/*******************************
Program name: DisplayNumInput
Main Method
program purpose: display unique
                 numbers
Programmer: Ihar Laziuk
Date created:03.03.2014
********************************/

public class DisplayNumInput{
   public static void main( String[] args ){
   
       //create a new object "num" using class NumInput
       NumInput num = new NumInput();
       num.findNum();//call the method from within NumInput class to prompt the user for entry and to display unique numbers
     } 
}