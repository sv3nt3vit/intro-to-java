/**********************************
Class name: DriverExam
Program purpose: compare student's
answers with the stored results
Programmer: Ihar Laziuk
Date created:03.03.2014
***********************************/

import java.util.*;
public class DriverExam{
   
   //create an array of the default correct answers
   char [] defaultAns={'B','D','C','D','A','D','B','A','C','B'};
   char [] studentAns;
   
   public DriverExam(char[] ans){
      studentAns=ans;
   }
   
   /*
      public method passed
      @param boolean returns true if passed
      @param returns false if not passed
   */
   public boolean passed(){
   
      if(totalCorrect()>=7)
         return true;
      else
         return false;
   }
   
    /*
      public method totalCorrect
      @param int
      @param returns number of questions answered correct
   */
   
   public int totalCorrect(){
   
      int ansCor=0;
      for(int i=0; i<defaultAns.length;i++){
      
         if(defaultAns[i]==studentAns[i])
            ansCor++;
      }
      return ansCor;
   }
   
   /*
      public method totalInCorrect
      @param int
      @param returns number of questions answered INcorrect
   */
   
   public int totalInCorrect(){

      return defaultAns.length-totalCorrect();
   }
   
     /*
      public method questionsMissed
      @param int
      @param returns numbers of the missed questions
   */
     
     public int [] questionsMissed(){
      
      int difference=defaultAns.length-totalCorrect();
      int [] questMissed=new int[difference];
      
      if(difference>0){
            for(int i=0; i<defaultAns.length; i++){
               int idx=0;//a var to keep track of the index of the incorrect answer, every loop reassign=0 to retrack
               if(defaultAns[i]!=studentAns[i]){
               idx+=i;//a var to keep track of the index of the incorrect answer
                  System.out.print("You have missed question:\n\t#"+idx);
                  System.out.println();
               }
            }        
      }                              
      return questMissed;   
   } 
      
 }   