/**********************************
Program name: DriverLicense
Program purpose: compare student's
answers with the stored results
Programmer: Ihar Laziuk
Date created:03.03.2014
***********************************/

import java.util.*;
public class DriverLicense{

   public static void main(String[]args){
   
      Scanner input=new Scanner(System.in);
      char [] studentAns=new char[10];//create an array to hold user's answers
      
      System.out.println("Enter your answer to the questions: \n"+
                         "Only letters 'A' 'B' 'C' 'D' accepted");  
      
      //read in the user's answers 
          
      for(int i=0; i<studentAns.length; i++){
         System.out.print(i+1+" ");      
         char in=Character.toUpperCase(input.next().charAt(0)); 
         studentAns[i]=in;
      }      

       System.out.println();
                     
       DriverExam exam=new DriverExam(studentAns);//create a new instance "exam" of a DriverExam class
       
       //call method "passed" and print the result passed/not passed  
       boolean ifPassed=exam.passed();
          if(exam.passed()==true)
             System.out.println("You have passed the exam!");
          else
             System.out.println("You haven't passed the exam");
       
       System.out.println("******************************");  
       System.out.println("You have "+exam.totalCorrect()+" correct answers");//call method "totalCorrect" for number of correct answers
         
       System.out.println("You have "+exam.totalInCorrect()+" incorrect answers");//call method "totalInCorrect" to get num of wrong ans

       exam.questionsMissed();//call method "questionsMissed to get numbers of questions that were missed
       System.out.println("******************************");     
   }
}
