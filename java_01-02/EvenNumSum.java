/*
Program name: EvenNumSum
Purpose: calculates the 
sum of all even numbers
2 to 100
Programmer: Ihar Laziuk
Date: 11.03.2013
*/

public class EvenNumSum
{
   public static void main(String[] args)
   {
      int sum=0;
      int i;
      
      System.out.println("This program will calculate the sum of all even numbers 2 to 100.");
      System.out.println("=================================================================");
      
      for (i=2; i<=100; i+=2)
      {
         sum+=i;
      }   
      
      System.out.println("The sum of all even numbers 2 to 100 equals to: "+sum+"\n\n\n");
      
/*
Program name: SquaresSum
Purpose: calculates the 
sum of all squares 
between 1 and 100
Programmer: Ihar Laziuk
Date: 11.03.2013
*/


      int sum1=0;
      int i1;
      
      System.out.println("This program will calculate the sum of all squares between 1 and 100");
      System.out.println("====================================================================");
      
      for(i1=1; i1<=100; i1++)
      {
         sum1+=i1*i1;
      }
         
      System.out.println("The sum of all squares between 1 and 100 equals to: "+sum1+"\n\n\n");
      
 /*
Program name: PowersOwTwo
Purpose: calculates 
powers of 2 from 2to0
up to 2to20
Programmer: Ihar Laziuk
Date: 11.03.2013
*/     
      
      
      double total;
      int i2, power=0;
      
      System.out.println("=================================================");
      System.out.println("This program will calculate all powers of 2 from \n2 to the power of 0 "+
      "up to 2 to the power of 20");
      System.out.println("=================================================");
      
      for(i2=0; i2<=20; i2++)
      {
         total= Math.pow(2,i);
         System.out.println("The power of \t2 to "+power+"\t equals: "+"\t"+total);
         power++;
      }    
      System.out.println("\n\n\n");     
/*
Program name: OddNumSum
Purpose: calculates the 
sum of all even numbers
2 to 100
Programmer: Ihar Laziuk
Date: 11.03.2013
*/
      int sum3=0;
      int i3;
      
      System.out.println("This program will calculate the sum of all odd numbers 1 to 100.");
      System.out.println("=================================================================");
      
      for (i3=1; i3<100; i3+=2)
      {
         sum3+=i;
      }   
      
      System.out.println("The sum of all even numbers 1 to 100 equals to: "+sum3);
           
 }
}