import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ExpandShrink extends JFrame{
  
   private JPanel panel;
   private JButton expandButton;
   private JButton shrinkButton;
   private int img_height=60;
   private int img_width=60;
  
   public ExpandShrink(){
      setTitle("Magic Button");
      setSize(300, 200);
     
     
      createComponents();
      setVisible(true);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
   }
  
   public void createComponents(){
  
      panel=new JPanel(); 
      
        //to add image
      ImageIcon icon=new ImageIcon("per.png");
      
       //to resize the image use 3 following lines of code, plus declare and initialize height and width;
      Image img = icon.getImage() ;  
      Image newimg = img.getScaledInstance( img_height, img_width,  java.awt.Image.SCALE_SMOOTH ) ;  
      icon = new ImageIcon( newimg );
      
      ButtonListener listener=new ButtonListener();

      expandButton=new JButton("Expand", icon);     
      expandButton.addActionListener(listener);  //register the listener to the button
     
      shrinkButton=new JButton("Shrink", icon);
      shrinkButton.addActionListener(listener);
           
      panel.add(expandButton);
      panel.add(shrinkButton);
     
      add(panel); //add panel to the frame
  
   }
  
   //inner class: a class inside another class
   private class ButtonListener implements ActionListener{
     
      public void actionPerformed(ActionEvent event){//register the listener for this button
         
         int width=getWidth();
         int height=getHeight();
         
      //use evt.getSource() returns the components generated event
      if(event.getSource()==expandButton){
      
         setSize((int)(width*1.1), (int)(height*1.1));
      }
      else
          setSize((int)(width*0.9), (int)(height*0.9));     
      }
   }
  
 
  
   public static void main(String [] agrs){
  
       ExpandShrink frame=new  ExpandShrink();
         
   }  
}