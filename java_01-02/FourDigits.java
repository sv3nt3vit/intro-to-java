/**
Program name: FourDigits
Purpose: converts a 4-digit
string number into decimal
Programmer: Ihar Laziuk
Date: 10.12.2013
*/

import java.util.*;

public class FourDigits
{
   public static void main(String [] args)
   {
      //Prompt the user to input a four digit number
      System.out.println("Please enter a four digit number");
      Scanner keyboard=new Scanner(System.in);
      String str=keyboard.nextLine();
      
      //Convert string to decimal
      int fourDigNum=Integer.parseInt(str);
      
      //Extract every number 
      int b0=str.charAt(0)-'0';
      int b1=str.charAt(1)-'0';
      int b2=str.charAt(2)-'0';
      int b3=str.charAt(3)-'0';
      
      //Do the math:))
      double result = 8*b0+4*b1+2*b2+b3;
      
      //Display the end result
      System.out.println("The result is: "+result);
    }
}

     