/****************************
Program Name: Game
Purpose: simulate realistic
game of Rock, Paper, Scissors
Programmer: Ihar Laziuk
Date: 11.28.2013
****************************/

import java.util.*;
public class Game{
   
   public static void main(String [] args){
   
      Scanner keyboard=new Scanner(System.in);
      String userChoice, compChoice;
      
   do{//this loop prompts to a new game while(userChoice.equals(compChoice))
      do{//validating user's entry. only 3 choices: paper, rock or scissors
         System.out.println("Enter your choice of rock, paper or scissors");
         userChoice = keyboard.next();}while(!userChoice.equals("rock")&&!userChoice.equals("paper")&&!userChoice.equals("scissors"));
   
         compChoice=random();//calling method "random" to generate computer's choice
         System.out.println("Computer's choice "+compChoice.toUpperCase());
   
      while(userChoice==compChoice){
         do{
         System.out.println("Enter your choice of rock, paper or scissors");
         userChoice = keyboard.next();}while(!userChoice.equals("rock")&&!userChoice.equals("paper")&&!userChoice.equals("scissors"));
   
         compChoice=random();
         System.out.println("Computer's choice "+compChoice.toUpperCase());
      }
         
        //calling method "winner" to determine the winner after choices from computer and user are received 
        winner(userChoice, compChoice);
              
         }while(userChoice.equals(compChoice));
  }   
   
   //create method to generate a random number 1 to 3
   public static String random(){
   
      /**
      random a string method to generate random answer
      @return "rock" for num 1
      @return "paper" for num 2
      @return "scissors" for num 3
      */

      Random rand=new Random();
      int num=rand.nextInt(3)+1;//specify the range
      
      //assign numbers to each rock, paper and scissor to employ random generated answer i this method
      int rock=1;
      int paper=2;
      int scissors=3;
   
      if(num==1)
         return "rock";
      else if(num==2)
         return "paper";
      else//num==scissors
         return "scissors";
   }  
   
   //this method "winner" determines the winner by comparing passed choices of the user and computer
   public static void winner(String userChoice, String compChoice){
   switch(userChoice){
         
            case "rock":
               if(compChoice=="scissors"){
                  System.out.println("You won!!!");
                  break;
              
               }
               else if(compChoice=="paper"){
                  System.out.println("Computer won:((");
                  break;
            
               }
               else{
                  System.out.println("\nOops, you both seem to have made the same choice\n\n\t\tLET'S PLAY AGAIN\n");
                  break;
               }
     
         case "scissors":
         
               if(compChoice=="rock"){
                  System.out.println("Computer won:((");
                  break;
               }
               else if(compChoice=="paper"){
                  System.out.println("You won!!!");
                  break;
               }
               else{
                  System.out.println("\nOops, you both seem to have made the same choice\n\n\t\tLET'S PLAY AGAIN\n");
                  break;}
         
         case "paper":          
         
               if(compChoice=="rock"){
                  System.out.println("You won!!!");
                  break;
               }
               else if(compChoice=="scissors"){
                  System.out.println("Computer won:((");
                  break;
               }
               else{
                  System.out.println("\nOops, you both seem to have made the same choice\n\n\t\tLET'S PLAY AGAIN\n");
                  break;}
        }
   }
} 
   
 
 
 
 
 
 
 
 
 
 
 
