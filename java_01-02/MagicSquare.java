/******************************
Program Name: MagicSquare
Purpose: fill an int array and
         check if nums form
         magic square
Programmer: Ihar Laziuk
Last updated: 2014.03.18
******************************/
import java.util.*;
public class MagicSquare{
   public static int [][]arr=new int[4][4];
   
   public static void main(String [] args){
   
      int sumOfRow=0;
      int sumOfCol=0;
      int sumOfDiag=0;
      int sumOfDiag2=0;
      
      for (int r=0; r<arr.length; r++){
         for(int c=0; c<arr[0].length; c++){               
            int temp=1+(int)(Math.random()*((16-1)+1));
            while(!isUnique(temp)){
               temp=1+(int)(Math.random()*((16-1)+1));
            }
               arr[r][c]=temp;
            System.out.print(arr[r][c]+" ");
         }
          System.out.println();
      }    
      System.out.println();//separator       
      
      //find sum of elements in each row      
      for(int r=0; r<arr.length;r++){
         sumOfRow=0;
         for(int c=0; c<arr.length; c++){
            sumOfRow+=arr[r][c];
         }
         System.out.println("Sum of row "+(r+1)+" = "+sumOfRow);;
      }
         
      System.out.println();//separator
         
      //find sum of elements in each column and print results
      for(int r=0; r<arr.length;r++){
         sumOfCol=0;
         for(int c=0; c<arr[r].length; c++){
            sumOfCol+=arr[c][r];
         }
         System.out.println("Sum of col "+(r+1)+" = "+sumOfCol);
      }     
      
      System.out.println();//separator
      
      //find sum of elemenst of left/right diagonals and print
      int c=arr.length-1; //set a column coordinate to find the sum of right diagonal
      for(int r=0; r<arr.length;r++){
         sumOfDiag+=arr[r][r]; //arr[r][r]-sum of the left diag;
         sumOfDiag2+=arr[r][c]; // arr[r][c]-sum of the right diagonal
         c--;//update column coordinate with every iteration
      }      
      System.out.println("Sum of left diagonal = "+sumOfDiag);  
      System.out.println("Sum of right diagonal = "+sumOfDiag2);
      
      //check if magic square and print
      if(sumOfRow==sumOfCol && sumOfCol==sumOfDiag){
         System.out.println("\nIt is a magic square!");
      }
      else
         System.out.println("\nNot a magic square this time");   
   }  
   
   //method to check if the number is duplicate or unique
   public static boolean isUnique(int e) {
        
   boolean found = false;
   int entry=e;

   for (int x = 0; x <= Math.pow(arr.length, 2); x++) {

       for (int i = 0; i < arr.length && found == false; i++) {
           for (int j = 0; j < arr.length; j++){
               if (arr[i][j] == entry){//check if array already contains entered num
                  found = true;
                  break;
               }
               else
                  found=false;
           }
       }          
    }
    if(found)
      return false;
    else
      return true;  
  }
}