/**
Program name: Marathon
Purpose: calculates 
sequence of runners
Programmer: Ihar Laziuk
Date created: 10.18.2013
Last updated: 10.25.2013
*/

import java.util.*;

public class Marathon
{
   public static void main(String [] args)
   {
      //prompt the user enter names of runners
      System.out.println("Please input name of the first runner");
      Scanner keyboard=new Scanner(System.in);
      String name1=keyboard.nextLine();
      System.out.println("Please input name of the second runner");
      String name2=keyboard.nextLine();
      System.out.println("Please input name of the third runner");
      String name3=keyboard.nextLine();
   
   
      //prompt the user enter time in minutes it took each to finish
   
      System.out.println("Enter time in minutes for "+name1+" - the 1st runner");
      int time1=keyboard.nextInt();
            
      System.out.println("Enter time in minutes for "+name2+" - the 2nd runner");
      int time2=keyboard.nextInt();
      
      System.out.println("Enter time in minutes for "+name3+" - the third runner");
      int time3=keyboard.nextInt();
      
      if(time1<time2 && time1<time3)//time1 is the fastest
      {
         if (time2<time3)
         {
            System.out.println("============================1st=========================");
         
            System.out.println("The first place takes: "+ name1+
                               " ,who finished in "+time1+" minutes"+"\nYahoo! Excellent result!\n");
            System.out.println("============================2nd=========================");
         
            System.out.println("The second place takes: "+ name2+
                               " ,who finished in "+time2+" minutes"+"\nGreat job!\n");
            System.out.println("============================3rd=========================");
         
            System.out.println("The third place takes: "+ name3+
                               " ,who finished in "+time3+" minutes"+"\nNot bad!");
            System.out.println("========================================================");
         }
         else //time1<time3<time2
         {
            System.out.println("============================1st=========================");
         
            System.out.println("The first place takes: "+ name1+
                               " ,who finished in "+time1+" minutes"+"\nYahoo! Excellent result!\n");
            System.out.println("============================2nd=========================");
         
            System.out.println("The second place takes: "+ name3+
                               " ,who finished in "+time3+" minutes"+"\nGreat job!\n");
            System.out.println("============================3rd=========================");
         
            System.out.println("The third place takes: "+ name2+
                               " ,who finished in "+time2+" minutes"+"\nNot bad!");
            System.out.println("========================================================");       
         }
      
      }
      else if(time2<time1 && time2<time3)//time2 is the fastest
      {
         if (time1<time3)//time2<time1<time3
         {   
            System.out.println("============================1st=========================");
         
            System.out.println("The first place takes: "+ name2+
                               " ,who finished in "+time2+" minutes"+"\nYahoo! Excellent result!");
            System.out.println("============================2nd=========================");
         
            System.out.println("The second place takes: "+ name1+
                               " ,who finished in "+time1+" minutes"+"\nGreat job!");
            System.out.println("============================3rd=========================");
         
            System.out.println("The third place takes: "+ name3+
                               " ,who finished in "+time3+" minutes"+"\nNot bad!");
            System.out.println("========================================================");
         }
         else //time2<time3<time1
         {
            System.out.println("============================1st=========================");
         
            System.out.println("The first place takes: "+ name2+
                               " ,who finished in "+time2+" minutes"+"\nYahoo! Excellent result!");
            System.out.println("============================2nd=========================");
         
            System.out.println("The second place takes: "+ name3+
                               " ,who finished in "+time3+" minutes"+"\nGreat job!");
            System.out.println("============================3rd=========================");
          
            System.out.println("The third place takes: "+ name1+
                               " ,who finished in "+time1+" minutes"+"\nNot bad!");
            System.out.println("========================================================");
         
         }   
      
      }
      else //time 3 is the fastest
      {
         if(time1<time2)//time3<time1<time2
         {
            System.out.println("============================1st=========================");
            System.out.println("The first place takes: "+ name3+
                               " ,who finished in "+time3+" minutes"+"\nYahoo! Excellent result!");
            System.out.println("============================2nd=========================");
         
            System.out.println("The second place takes: "+ name1+
                               " ,who finished in "+time1+" minutes"+"\nGreat job!");
            System.out.println("============================3rd=========================");
         
            System.out.println("The third place takes: "+ name2+
                               " ,who finished in "+time2+" minutes"+"\nNot bad!");
            System.out.println("========================================================");
         }
         else//time3<time2<time1
         {
            System.out.println("============================1st=========================");
            System.out.println("The first place takes: "+ name3+
                               " ,who finished in "+time3+" minutes"+"\nYahoo! Excellent result!");
            System.out.println("============================2nd=========================");
         
            System.out.println("The second place takes: "+ name2+
                               " ,who finished in "+time2+" minutes"+"\nGreat job!");
            System.out.println("============================3rd=========================");
         
            System.out.println("The third place takes: "+ name1+
                               " ,who finished in "+time1+" minutes"+"\nNot bad!");
            System.out.println("========================================================");
         }}
   }
}

      

      
