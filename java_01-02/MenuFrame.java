import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;

public class MenuFrame extends JFrame {
   
   //======== fields (global variables)=========
   private JMenuBar menuBar;
   private JMenuItem openItem;
   private JMenuItem closeItem;
   
   private JRadioButtonMenuItem redButton;
   private JRadioButtonMenuItem greenButton;
   private JRadioButtonMenuItem blueButton;
   
   private JPanel panel;
   private JLabel label;
   
   private JCheckBoxMenuItem visibleBox;
      
   
   public MenuFrame(){
      setTitle("Menu Demo");
      setSize(500, 400);
      
      menuBar = new JMenuBar();
      setJMenuBar(menuBar);
     
     
      createComponents();
      //pack();
     
      setVisible(true);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
   }
   
   public void createComponents(){
      buildFileMenu();
      buildTestMenu();
      
      panel = new JPanel();
      label = new JLabel("Finally, it's done");
      
      panel.add(label);
      add(panel);
   
   }
   
   
   public void buildFileMenu(){
   
      MenuListener listener = new MenuListener();
      
      JMenu fileMenu = new JMenu("File"); 
      fileMenu.setMnemonic(KeyEvent.VK_F);
      
      openItem = new JMenuItem("Open");
      openItem.setMnemonic(KeyEvent.VK_O);
      openItem.addActionListener(listener);
      
      closeItem = new JMenuItem("Close");
      closeItem.setMnemonic(KeyEvent.VK_C);
      closeItem.addActionListener(listener);
      
    
      
      fileMenu.add(openItem);
      fileMenu.addSeparator();   //add a line separator
      fileMenu.add(closeItem);
          
      menuBar.add(fileMenu);
    
   }
   
   
   public void buildTestMenu(){
   
      JMenu testMenu = new JMenu("Test");
      testMenu.setMnemonic(KeyEvent.VK_T);
      
      MenuListener listener = new MenuListener();
      
      redButton = new JRadioButtonMenuItem("Red");
      redButton.addActionListener(listener);
      redButton.setMnemonic(KeyEvent.VK_R);
      
      greenButton = new JRadioButtonMenuItem("Green");
      greenButton.addActionListener(listener);
      greenButton.setMnemonic(KeyEvent.VK_G);
      
      blueButton = new JRadioButtonMenuItem("Blue");
      blueButton.addActionListener(listener);
      blueButton.setMnemonic(KeyEvent.VK_B);
      
      ButtonGroup group = new ButtonGroup();
      
      group.add(redButton);
      group.add(greenButton);
      group.add(blueButton);
      
      visibleBox = new JCheckBoxMenuItem("Visible");
      visibleBox.addActionListener(listener);
      
      testMenu.add(redButton);
      testMenu.add(greenButton);
      testMenu.add(blueButton);
      testMenu.addSeparator();
      testMenu.add(visibleBox);
      
      menuBar.add(testMenu);
   
   }
   
   private class MenuListener implements ActionListener{
      //override actionPerformed method imlemented
      public void actionPerformed (ActionEvent event){
          if(event.getSource()==openItem){
            //JOptionPane.showMessageDialog(null, "Open a file");
            
            JFileChooser chooser = new JFileChooser();
            int status = chooser.showOpenDialog(null); // will pop up the save as 
               if(status == JFileChooser.APPROVE_OPTION){
                  File file = chooser.getSelectedFile();
                  String fileName = file.getName();
                  setTitle(fileName);
                  String filePath = file.getPath();
                  JOptionPane.showMessageDialog(null, filePath);
                  
                  //          ======================= TO DO =======================
                  //should have TextArea to display the contents of the file (chapter 4)
                  //Use Scanner IO until hasNext keep reading +/- scrollBar

               }
          }
          else if (event.getSource()==closeItem) {
            //JOptionPane.showMessageDialog(null, "Close a file");  
            setTitle("");  //remove the title from TitleBar  
            //next should clear the area       
          }
          else if (redButton.isSelected()){
            label.setForeground(Color.RED);
          }
          else if (greenButton.isSelected()){
            label.setForeground(Color.GREEN);
          }
          else if (blueButton.isSelected()){
            label.setForeground(Color.BLUE);
          }
          if (visibleBox.isSelected()){
            label.setVisible(false);
          }
          else
            label.setVisible(true);
      }
   
   }
   
   public static void main (String[] args){
   
      MenuFrame menuFrame = new MenuFrame();
   }
   
}