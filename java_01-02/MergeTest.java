/*******************************
Program Name: MergeTest
Purpose: creates and merges
         two array lists of
         data type int into one 
Programmer: Ihar Laziuk
Last updated: 2014.03.18
********************************/
import java.util.*;
public class MergeTest{  
   public static void main(String[]args){
   
      //create two arrayLists
      ArrayList<Integer>list1=new ArrayList<>();
      ArrayList<Integer>list2=new ArrayList<>(); 
      Scanner in=new Scanner(System.in);
      int entry;//decalre var entry for the further use, to exit when array content has been entered  
      
      System.out.println("Let's build two arrays");
      System.out.println("**********************");
      System.out.println("Enter as many integer numbers for array # 1 as you want:\nwhen done type -1");

      //prompt user enter numbers for arrayList #1
         do{
            entry=in.nextInt();           
               if(entry!=-1)
                  list1.add(entry);
         }while (entry!=-1);            
      System.out.println("Here is your array #1 "+list1);;
        
       //prompt user enter content for arrayList #2
       System.out.println("\nNow enter numbers for ARRAY # 2:\nwhen done entering type -1"); 
         do{
            entry=in.nextInt();           
               if(entry!=-1)
                  list2.add(entry);
         }while (entry!=-1);            
      System.out.println("Here is your array #2 "+list2);   
      
      //call merge() method, passing to it two arguments: two ArrayLists
      System.out.print("\nHere is the new merged array: ");
      System.out.println(merge(list1, list2));                  
   }
   
   /**
   Method name: merge, takes two args
   @param Arraylist one - first created ArrList
   @param Arraylist two - second created ArrList
   @param ArrayList - new merged ArrList to be returned
   */
   public static ArrayList merge(ArrayList one, ArrayList two){
      
   int i = 0, j = 0; //current index positions for arrLists one & two 
   ArrayList<Integer> list3 = new ArrayList<Integer>();//create a new ArrList to hold the content of 2 passed arrays 

   while(i < one.size() || j < two.size()) {//use .size() instead of .length with ArrayList
      if(i < one.size())
         list3.add((Integer) one.get(i++));//if still hasNext add element to new arrList and increment position
      if(j < two.size())
         list3.add((Integer) two.get(j++));//if still hasNext add element to new arrList and increment position
   }
    return list3;       //return the new merged arrayList      
   }
}