
public class MultTable{

//declare a 2D array
  private int[][]table;
      
      public MultTable(int row, int col){
      
        table=new int[row][col];
        
        //populate the values to the array
        //each element's value is row Index*colIdx
        
         for (int r=0; r<table.length; r++){//rows
            for(int c=0; c<table[0].length; c++)
              
              table[r][c]=r*c; 
         }
   }    
      public void displayTable(){
      
         for (int r=0; r<table.length; r++){
            for(int c=0; c<table[0].length; c++)
               System.out.print(table[r][c]+"\t");
               
            System.out.println();
         }  
     }    
}
            
         
      