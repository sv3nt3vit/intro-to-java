/*******************************
Class NumInput
Program name: NumInput
program purpose: display unique
                 numbers
Programmer: Ihar Laziuk
Date created:03.03.2014
********************************/
import java.util.*;
 
public class NumInput{
 
   public void findNum(){
   
      Scanner input = new Scanner(System.in);
   	int[] arr = new int[5];
      int num;
   	int cnt= 0;
   	int entry= 0;//variable to update number of the valid entries only
    
   	while (entry < arr.length) {
      
   		System.out.println("Enter a number > 10 and < 100");
   		num = input.nextInt();
    
   		if ((num >= 10) && (num <= 100)) {//checking for valid entry 10<=num<=100
   			boolean isDuplicate = false;
   			entry++;//update number of valid entries when the check passed
    
   			for (int i=0; i<arr.length; i++) {
   			   if (arr[i] == num)		
   			      isDuplicate = true;
   			}	
    
   			if (!isDuplicate) {
   			   arr[cnt] = num;
   			   cnt++;
   			} 
    
   			else    
   			   System.out.println("The number "+num+" is duplicate");   
   	    }  
   		   else
   		      System.out.println("Cannot be < 10 and > 100");
    
   		//print entered number
         for (int i=0;  i<entry; i++) { 
            if (arr[i]!=0)//use this statement to prevent from printing. When entered duplicate number instead of
                          //not being printed at all it will be automatically reset to "0" (not quite sure why). So don't print it          
            System.out.print(arr[i] + " ");
   		}
            
         System.out.println();    
   	}
   } 
} 