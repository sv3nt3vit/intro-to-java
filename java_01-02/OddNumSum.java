/*
Program name: OddNumSum
Purpose: calculates the 
sum of all even numbers
2 to 100
Programmer: Ihar Laziuk
Date: 11.03.2013
*/

public class OddNumSum
{
   public static void main(String[] args)
   {
      int sum=0;
      int i;
      
      System.out.println("This program will calculate the sum of all odd numbers 1 to 100.");
      System.out.println("=================================================================");
      
      for (i=1; i<100; i+=2)
      {
         sum+=i;
      }   
      
      System.out.println("The sum of all even numbers 1 to 100 equals to: "+sum);
   }
}
      