import java.util.*;

public class Palindrome{
   public static void main(String []args){
   
   Scanner key=new Scanner(System.in);
   System.out.println("Enter a word or a phrase:");
   
   String entry=key.nextLine();
   
   StringBuilder strbld=new StringBuilder (entry);
  
   String str=strbld.toString();
   StringBuilder newStr=strbld.reverse();
   
   System.out.println(str);
   
   String str2=newStr.toString();
   System.out.println(str2);
   
   if(str.equals(str2))
      System.out.println("It is a palindrome");
   else
      System.out.println("Not a palindrome");   
   }
}