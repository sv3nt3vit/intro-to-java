import java.util.*;
public class PalindromeEfficient{
   public static void main(String []args){
   
   System.out.println("Enter a word\nand I will check if it is a palindrome: ");
   Scanner in=new Scanner(System.in);
   
   String entry=in.next();
   String revEntry=new StringBuilder(entry).reverse().toString();
   
   if(entry.equals(revEntry)){
       System.out.println("This is palindrome");
   } else
       System.out.println("It is not a palindrome");
   }
}