/***********************
Program Name: Password 
Purpose:validate and 
        confirm psw 
Programmer: Ihar Laziuk
Date: 11.28.2013
************************/

import java.util.*;

public class Password{
   public static void main(String [] args){
   
   Scanner keyboard=new Scanner(System.in);
   String psw, confirmPsw;
   String match="Password is valid";
   
   do{//this loop validates and prompts the user enter new psw every time it does not fullfill all the requirements
      System.out.println("=================================================================================");
      System.out.println("Enter desired password");
      System.out.println("Make sure:\n\t1. The password must be at least 8 characters long"+
                     "\n\t2. The password must have at least one uppercase and one lowercase letter"+
                     "\n\t3. The password must have at least one digit");
      System.out.print("=================================================================================\nPassword: ");
      psw=keyboard.next();
      
      System.out.println(validation(psw));
    }while (validation(psw)!="Password is valid");
    
    do{//this do...while loop prompts the user to confirm entered psw until both passwords match. Unlimited number of attempts
      System.out.println("Confirm entered password");
      confirmPsw=keyboard.next();
    
      System.out.println(pswMatch(psw, confirmPsw));
    }while (pswMatch(psw, confirmPsw)!="Success! Passwords match");
  }
   
   //method validates desired entered password.
   //psw should contain 1 digit, 1 uppercase, 1 lowercase letter and should be at least 8 characters long
   public static String validation(String psw){
   
     /**
      validation a string method, checking the fullfillment of all requirements
      @param psw is a String "rock" for num 1
      @returns matching message
      */

   String match="Password is valid";
   int cntDigit=0;
   int length = 0;
   int cntUp=0;
   int cntLow=0;
   
   for(int i=0; i<psw.length(); i++){
   
      if((psw.charAt(i)>=48 && psw.charAt(i)<=57))//check if the psw contains at least 1 digit
         cntDigit++;
         
      if((psw.charAt(i)>=65 && psw.charAt(i)<=90))//check if the psw contains at least 1 Uppercase letter
         cntUp++;
         
      if((psw.charAt(i)>=97 && psw.charAt(i)<=122))//check if the psw contains 1 Lowercase letter
         cntLow++;
      }
      
      //return value if one of the parameters is not fullfilled
      if(cntDigit<1)
         match="Password must contain at least ONE DIGIT";
      
      if(cntUp<1)
         match="Password must contain at least ONE UPPER CASE letter";
         
      if(cntLow<1)
         match="Password must contain at least ONE LOWER CASE letter";
      
      if(psw.length()<7)
         match="Password should be at least 8 characters LONG";
      
      return (match);

   }
   
   //this method checks whether passwords match
   public static String pswMatch(String psw, String confirmPsw){
   
     /**
      pswMatch a string method, checking whether both passwords match
      @param psw is a string, entered user's choice
      @param confirmPsw is a String, confirmation password entered by the user
      @returns a String whether both passwords match or not 
      */
      
      if(psw.equals(confirmPsw))
         return "Success! Passwords match";
      else
         return "Passwords DO NOT match";
   }
}   
   
   
   
   
   
   
   
