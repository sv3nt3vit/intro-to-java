/**
Program Name: Points
Purpose: calculates
points awarded for the
purchase
Programmer: Ihar Laziuk
Date: 10.26.2013
*/

import java.util.*;

public class Points
{
   public static void main(String [] args)
   {
      int points = 0;
      
      System.out.println("Please enter the number of books purchased this month");
      Scanner keyboard=new Scanner(System.in);
      int books=keyboard.nextInt();
                  
      switch (books)
         {
            case 0:
                  points=0;
                  System.out.println("You now have "+points+" points");
                  break;
            case 1:
                  points=5;
                  System.out.println("You now have "+points+" points");
                  break;
            case 2:
                  points=15;
                  System.out.println("You now have "+points+" points");
                  break;
            case 3:
                  points=30;
                  System.out.println("You now have "+points+" points");
                  break;
            case 4:
                  points=60;
                  System.out.println("You now have "+points+" points");
                  break;
            default:
                  points=60;
                  System.out.println("You now have "+points+" points");
                  break;
           }                          
     }
}
                        

