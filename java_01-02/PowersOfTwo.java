/*
Program name: PowersOwTwo
Purpose: calculates 
powers of 2 from 2to0
up to 2to20
Programmer: Ihar Laziuk
Date: 11.03.2013
*/

public class PowersOfTwo
{
   public static void main(String [] args)
   {
      double total;
      int i, power=0;
      
      System.out.println("=================================================");
      System.out.println("This program will calculate all powers of 2 from \n2 to the power of 0 "+
      "up to 2 to the power of 20");
      System.out.println("=================================================");
      
      for(i=0; i<=20; i++)
      {
         total= Math.pow(2,i);
         System.out.println("The power of \t2 to "+power+"\t equals: "+"\t"+total);
         power++;
      }    
   }
}