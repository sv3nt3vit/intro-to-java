/*
Program Name: PrimeNumbers
Purpose: save the first 100
prime numbers to a file
Programmer: Ihar Laziuk
Date: 11.13.2013
Last Update: 11.16.2013
*/
import java.io.*;

public class PrimeNumbers
{  
   //for readability purpose create a boolean method to find prime nnumbers
   /**
   Method to find prime numbers
   @param i
   @return boolean ifPrimeNum
   */  
   public static boolean ifPrimeNum(int i)
   {    
         for (int e=2; e<=i/2; e++)//for effeciency purposes checking only one half of the numbers using algebraic approach
            {
               if(i%e==0)
                  {
                     return false;
                  }
             }
             return true;
    }

   public static void main(String [] args) throws IOException
   {
     PrintWriter out=new PrintWriter(new FileWriter("C:/CSC110/prime.txt"));//create a new file. Destination folder can be changed
     int count=0;
     int cnt=0;
     int i=2;
     
     while (count<100)
      {
         
           if (ifPrimeNum(i))
           {    
     
                  System.out.print(i+" ");//will display for the sole purpose of demonstration
                                          //same content being saved to "prime.txt"
                  out.print(i+" ");//save to a file created before
                  cnt++;
                  count++;
                
                //reset cnt every time it reaches 5 to switch to a new line
                  if   (cnt==5) 
                  {
                     System.out.println();
                     out.println();
                     cnt=0;
                  }              
           }    
            i++;
       }
         out.close();
   }
}

