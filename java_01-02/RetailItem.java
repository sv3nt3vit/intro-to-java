/*
Class Name: RetailItem
Purpose: to be used with
class CashRegister, 
provides methods to set
and acccess values of
the items
Programmer: Ihar Laziuk
Last Updated: 2014.31.03
*/
public class RetailItem{

   private String description;
   private int unitsOnHand;
   private double price;  
   
   //constructor to initialize
   public RetailItem(String desc, int quant, double price){
      setDesc(description);
      setQuant(unitsOnHand);
      setPrice(price);
   }
   
   //set item name
   public void setDesc(String desc){
      description=desc;
   }
   
   //set quantity of the item
   public void setQuant(int quant){
      unitsOnHand=quant;
   }
   
   //method to set the price of the item
   public void setPrice(double p){
      price=p;
   }

   //access price
   public String getDesc(){
      return description;
   }
   
   //get quantity of the item
   public int getQuant(){
      return unitsOnHand;
   }
   
   //get price of the item
   public double getPrice(){
      return price;
   }
}