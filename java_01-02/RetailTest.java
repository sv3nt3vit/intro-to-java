/*
Class Name: RetailTest
Purpose: tuns CashRegister,
RetailItem classes to
simulate slaes
Programmer: Ihar Laziuk
Last Updated: 2014.31.03
*/
import java.util.*;
import java.io.*;

public class RetailTest{
   public static void main(String []args)throws IOException{
   
      double subTotal, salesTax, total;
                          
      //create 3 objects - items and pass arguments
      RetailItem jacket=new RetailItem("Jacket", 12, 59.95);
      RetailItem jeans=new RetailItem("Designer Jeans", 40, 34.95);
      RetailItem shirt=new RetailItem("Shirt", 20, 24.95);
      
      Scanner key=new Scanner(System.in);
      
      System.out.println("How many items did you purchase?");
      int numOfItems=key.nextInt();
      
      CashRegister register=new CashRegister(jeans, numOfItems);
      
      register.printReceipt();
            
      //write to a file
      File file=new File("C:\\receipt.txt");
      PrintWriter writer=new PrintWriter("receipt.txt");
      
      writer.println("The sub total is "+register.getSubtotal()+"\n"+
                         "sales tax is "+register.getTax()+"\nThe total is "+register.getTotal());
      writer.close();
  
                             
   }
   
}