
public class SalesReceipt{

   private int numOfItems;
   private RetailItem item;
   CashRegister register=new CashRegister(item, 1);

    public SalesReceipt(RetailItem unit, int num){
      item= unit;
      numOfItems=num;
    } 
    
    public void printReceipt(){
    System.out.println("SALES RECEIPT\n"+
                       "Unit Price: "+item.getPrice()+
                       "\nQuantity: "+item.getQuant()+
                       "\nSubtotal: "+register.getSubtotal()+
                       "\nSales Tax: "+register.getTax()+
                       "\nTotal: "+register.getTotal());  
    }
}