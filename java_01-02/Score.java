/*
Program's Name: Score
Program's Purpose: validate
entry for a range 1-100
Last Updated: 11.01.2014
Programmer's Name: Your Name:) here
Have fun with java
**/

import java.util.*;

public class Score {

   public static void main(String[] args)throws InputMismatchException {

   System.out.println("Enter your score 1 to 100");
   Scanner sc = new Scanner(System.in);
   int score;
   
   do{
   
      if (sc.hasNextInt()) {
          score = sc.nextInt();
          if (score < 1 || score > 100)
         System.out.println("Error, enter an integer 1 to 100");

      } else {
          sc.next();
          score = 0;
          System.out.println("Invalid entry! Enter your score 1 to 100: ");
      }
   
   }while (score < 1 || score > 100); 

  }
}