/**
Program name: ShippingCharges
Purpose: calculates 
shippong charges
Programmer: Ihar Laziuk
Date created: 10.26.2013
*/

import java.util.*;
import java.text.*;

public class ShippingCharges
{
   public static void main(String [] args)
   {
      double weight, calcDistance, distance;
      double charge=0;
      int count=0;
      DecimalFormat df= new DecimalFormat("0.00"); 
      
      //prompt the user enter weight and distance in miles and store the data to the memory
      System.out.println("Enter the weight of the package in pounds");
      Scanner keyboard=new Scanner(System.in);
      weight=keyboard.nextDouble();
      
      System.out.println("Enter the shipping distance in miles");
      distance=keyboard.nextDouble();
      calcDistance=distance;
      
      
      while (calcDistance>0)//to get the count of miles with incrememnt 500 not prorated
      {
         calcDistance-=500;
         count++;
      }
      
      if(weight<=2)//calculate the charge for shipping depending on the weight and distance
      {
         charge=count*1.10;
      }
      else if(weight>2 && weight<=6)
      {
         charge=count*2.20;
      }
      else if(weight>6 && weight <=10)
      {
         charge=count*3.70;
      }
      else
         charge=count*3.80; 
         
      //display total charges according to the value of weight and distance entered by the user. Only 2 digits of precision will be displayed
      System.out.println("The cost of "+weight+" lbs to send "+distance+" miles is: $ "+df.format(charge));
   }
}
      
      