/********************************************
Program name: SineValue
Purpose: Computes the sine, the cosine of a
double precision value and the sum of 2 sqrt
Programmer: Ihar Laziuk
Date of creation: 09.25.2013
Last update: 10.03.2013
********************************************/

import java.util.*;
public class SineValue
{
   public static void main(String [] args)
   {      
      //1.Display"Enter value in radians"
      System.out.println("Enter value in radians");
      
      //2.Prompt the user to input value and save it to the memory
      Scanner keyboard=new Scanner(System.in);
      double radians=keyboard.nextDouble();
      
      //3.Calculate sine and save in to the memory
      double sinx = Math.sin(radians);
           
      //4.Calculate cosine and store in to the memory
      double cosx= Math.cos(radians);
           
     //5.Calculate the square of sine and cosine and add squares
     double squareSum=Math.sqrt(sinx)+Math.sqrt(cosx); 
     
     //6.Display sin x and cos x values and the sum of the their square roots
     System.out.println("=========================================================================="+
     "\nThe sine of "+radians+" radians is: "+sinx+
      "\n\nThe cosine of "+radians+" radians is: "+cosx+
      "\n\nThe sum ot the two square roots of sine and cosine is: "+squareSum+
      "\n==========================================================================");
    }
}