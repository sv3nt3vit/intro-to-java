/*
Program name: SquaresSum
Purpose: calculates the 
sum of all squares 
between 1 and 100
Programmer: Ihar Laziuk
Date: 11.03.2013
*/

public class SquaresSum
{
   public static void main(String [] args)
   {
      int sum=0;
      int i;
      
      System.out.println("This program will calculate the sum of all squares between 1 and 100");
      System.out.println("====================================================================");
      
      for(i=1; i<=100; i++)
      {
         sum+=i*i;
      }
         
      System.out.println("The sum of all squares between 1 and 100 equals to: "+sum);
   }
}