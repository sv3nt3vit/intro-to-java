/*
Program Name:StockPrice
Purpose: reports when
the target is reached
Programmer: Ihar Laziuk
Date: 11.07.2013
*/

import java.util.*;

public class StockPrice
{
   public static void main(String [] args)
   {
      double stockPrice;
      boolean found=false;
      
      Scanner keyboard=new Scanner(System.in);
      
      System.out.println("Enter the name of the company that you own some shares of stock");
      String name=(keyboard.nextLine().toUpperCase());
      System.out.println("Enter the target price for the stock and I will inform you when it is reached");
      double targetPrice=keyboard.nextDouble();
      
      do
      {
         System.out.println("Enter the stock price");         
         stockPrice=keyboard.nextDouble();
         
         if(stockPrice<=0)
         do
         {
            System.out.println("You have entered invalid value, try again: price may only be positive\n");
            System.out.println("Enter the stock price");
            stockPrice=keyboard.nextDouble();
         }while(stockPrice<=0);
         
         if (stockPrice==targetPrice)
            found=true;
      }while(!found&&stockPrice<targetPrice);
      
      if(stockPrice==targetPrice)
         System.out.println("\nYahhoo!\tCongratulations!\n\nYou are about to make some $$$ gain $$$ in "+name+" - "+
                        "your stock has reached its target price!\n\nNow it is the time to cover your position!");
      else
         System.out.println("\nYahhoo!\tIt is the time to sell\n\n"+name+"'s stock price exceeds the target price");
   }
}
      
      