/*****************************
Program's name: SumOfInt
Program's purpose: calculate
the sum and percentage of the
contribution of entered int-s

Programmer: Ihar Laziuk
Date: 02.16.2014
******************************/

import java.util.*;
import java.text.*;

public class SumOfInt {

   public static void main(String [] args){

      System.out.println("Enter desired quantity of numbers");
      Scanner keyboard=new Scanner (System.in);
      int size=keyboard.nextInt();
      
      int  [] numbers=new int [size];
      
      System.out.println("Now enter "+size+" integers");
      
      //read in numbers
      for(int i=0; i<numbers.length; i++)
         numbers[i]=keyboard.nextInt();
      
      //calculate the sum of entered integers
      int sum=0;   
      for(int i=0; i<numbers.length; i++){
         sum+=numbers[i];
      }
      
      System.out.println("******************************");
      System.out.println("The sum is: "+sum+"\n");
      
      System.out.println("The numbers are:\n");
      
      DecimalFormat df=new DecimalFormat("#.###");
      
      //print entered integers and calulate their contribution in % to the sum, use format for 3 digits precision
      for(int i=0; i<numbers.length; i++){
         System.out.println(numbers[i]+"\t"+df.format((100.0/sum)*numbers[i])+"% of the sum.\n");
      }
       System.out.println("******************************");
 
  }
}