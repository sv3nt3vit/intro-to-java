/****************************
Program Name: Syllables
Purpose: counts syllables
in the given list of words
Programmer: Ihar Laziuk
Date: 02.05.2014
****************************/
import java.io.*;
import java.util.*;
public class Syllables{
     
   
  public static void main (String []args)throws IOException {
   
   FileReader file=new FileReader("dictionary.txt");
   Scanner inputFile=new Scanner(file);
 
   
   PrintWriter out=new PrintWriter(new FileWriter("C:/CSC210/syllables.txt"));
  
   while(inputFile.hasNext()){
         String w=inputFile.next();
         out.println(countSyllables(w)+" ");
         
      }
      
      inputFile.close();

      out.close();
   
   }  

   /** 
     Check if a character is a vowel or not.
     @param ch char
     @return if ch is a vowel or not.
   */
   
   private static boolean isVowel(char ch){
   
     String temp=(ch+"").toLowerCase();
     ch=temp.charAt(0);
      
      switch(ch)
      {
         case 'a': case 'e': case 'i': case 'o':
         case 'u': case 'y': return true;
         default:  return false;
      }
   
   }
   
   /** 
     Count syllables    
     Check if the last character is a vowel or not.
     @param int cntOfSyllables
     @param boolean isLastVowel
     @return if ch is a vowel or not.
   */
         
    private static int countSyllables(String word) {
    int cntOfSyllables = 0;

    
    boolean isLastVowel = false;
    
    for (int i = 0; i < word.length(); i++) {
    
       if (word.contains("a") || word.contains("e") || word.contains("i") || word.contains("o") || word.contains("u")) {
             if (isVowel(word.charAt(i)) && !((word.charAt(i) == 'e') && (i == word.length()-1))) {
                
                if (isLastVowel == false) {
                    cntOfSyllables++;
                    isLastVowel = true;
                }
            } 
            
            else {
                isLastVowel = false;
            }           
        }
        
         else {
            cntOfSyllables++;
            break;
        }
    }
    return cntOfSyllables;
   }
 }


