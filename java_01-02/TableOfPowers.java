/*
Program Name: TableOfPowers
Purpose: creates a table of
powers of entered values
Programmer: Ihar Laziuk
Date: 11.13.2013
*/
import java.util.*;

public class TableOfPowers
{
   public static void main(String [] args)
   {
      System.out.println("Enter the number of rows:");//prompt the user enter the number of rows
      Scanner keyboard =new Scanner(System.in);
      int row=keyboard.nextInt();//save entered value to the memory
     
      System.out.println("Enter the number of columns:");//pronmpt the user enter the number of columns
      int column=keyboard.nextInt();//save entered value to the memory     
      
      System.out.print("X|\t");//create and print the upper border and column headers 
         for(int c=1; c<=column; c++)        
            System.out.print("X"+"^"+c+"\t\t");
         System.out.println();
         
         
         for (int c=1; c<=column; c++)//create and print the divider for the upper border
            System.out.print("----------------");
         System.out.println();
         
         for (int r=1; r<=row; r++)//create and print number for each row and divider
         {
             System.out.print(r+"| ");

            for (double c=1; c<=column; c++)//controls the number of columns
               System.out.print("\t"+Math.pow(r,c)+"\t");//calculate 'r'to the power of 'c')
               System.out.println();
         }
   }
}   