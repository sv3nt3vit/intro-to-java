/*
Program Name: Temperature
Purpose: finds the highest
temperature of the entered
values and # of the month
Programmer: Ihar Laziuk
Date: 11.03.2013
*/

import java.util.*;

public class Temperature
{
   public static void main(String [] args)
   {
      Scanner keyboard=new Scanner(System.in);
      int maxPos=0;
      double temp;
      int i;
      double maxTemp=0;
      
      System.out.println("======================================================="); 
      System.out.println("Enter temperature in Celsius for each of the 12 months:");
      System.out.println("=======================================================\n"); 

      System.out.print("Month 1: \n");
      temp=keyboard.nextDouble();
      
      for (i=1; i<12; i++)
      { 
         System.out.println("Month "+(i+1)+": ");
         temp= keyboard.nextDouble();
   
         if(temp>maxTemp)
         {
         maxTemp=temp;
         maxPos=i;
         }
      }
      
      System.out.println("The max temperature was "+maxTemp+"'C in the month "+(maxPos+1));
    }
}