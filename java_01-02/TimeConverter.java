/*******************************************
Program name: TimeConverter
Purpose: Convertion of hours and min to sec
Programmer: Ihar Laziuk
Date of creation: 09.25.2013
Last update: 10.03.2013
*******************************************/

import java.util.*;
public class TimeConverter
{
   public static void main(String [] args)
   {  
           
      //1.Display "Enter desired value of hours"
      System.out.println("Enter desired value of hours");
      
      //2.Let the user input value of hours and store it to the memory
      Scanner keyboard=new Scanner(System.in);
      int hours=keyboard.nextInt();
      
      //3.Display enter desired value of minutes
      System.out.println("Enter desired value of minutes");
      
      //4.Llet the user input value of minutes and store it to the memory
      int minutes=keyboard.nextInt();
      
      //5.Display enter desired value of seconds
      System.out.println("Enter desired value of seconds?");
      
      //6.Let the user input value of seconds and store it to the memory
      int seconds=keyboard.nextInt();
      
      int hourSec = hours*60*60;
      int minSec=minutes*60;
      int totalSec;

      
      //7.Perform convertion to seconds
      totalSec= hourSec+minSec+seconds;
      
      //8.Display total value in seconds
      System.out.println("The equivalent value in seconds is "+totalSec);
      }     
}
