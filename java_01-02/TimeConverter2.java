/*******************************************
Program name: TimeConverter2
Purpose: Convertion of seconds to hours, min
Programmer: Ihar Laziuk
Date of creation: 09.25.2013
Last update: 10.03.2013
*******************************************/

import javax.swing.*;
public class TimeConverter2
{
   public static void main(String [] args)
   {      
      //1.Prompt the user to enter desired value in seconds
      String input=JOptionPane.showInputDialog("Enter desired value in seconds");
      int seconds=Integer.parseInt(input);
      
      //2.Get the hours position
      int hours=seconds/3600;
      int remainder=seconds%3600;
      
      //3.Get the minutes position
      int minutes=remainder/60;
      remainder=remainder%60;
      
      //4.Get the sesconds position
      seconds=remainder;
                 
      //5.Display the equivalent to entered seconds in hours, minites and seconds
      JOptionPane.showMessageDialog(null, "The equivalent value to entered seconds is:\n "+
                                    "==========\t"+ hours+ " \thours========= \n"+"========="+ minutes+
                                    " minutes========\n"+"========="+ + seconds + " seconds========");
      }     
}
