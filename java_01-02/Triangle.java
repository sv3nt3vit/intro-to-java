/*
Program Name: Triangle
Purpose: detirmines type
         of triangle
Programmer: Ihar Laziuk
Last Updated: 2014.31.03
*/

import java.util.*;

public class Triangle {
	
	public enum triangleType{SCALENE, ISOCELES, EQUILATERAL, NOTRIANGLE}	 
   
	//method to detirmine triangles shape
   public static triangleType triangleShape(int s1, int s2, int s3) {
		triangleType type = null;//have to initialize.. set to null
		
		if(s1 <= 0 || s2 <= 0 || s3 <= 0){
			type = triangleType.NOTRIANGLE;
		} else if(s1!=s2 && s2!=s3 && s1!=s3) {
			type = triangleType.SCALENE;
		} else if(s1==s2 && s2==s3 && s3==s1) {
			type = triangleType.EQUILATERAL;
		} else if(s1==s2 || s2==s3 || s1 == s3) {
			type = triangleType.ISOCELES;
		}		
		return type;
	}
      
   public static void main(String[] args){
   
		Scanner key=new Scanner(System.in);
		System.out.println("Enter the length of the sides: \n");
      
      int[] sides = new int[3];//stores the sides in an arr instead of creating new variables
      
      for(int i=1; i<=3; i++){
         System.out.print("Side " + i+" ");
		   sides[i-1] = key.nextInt();
      }
		
		//call the function and pass arguments  sides
      triangleType type = triangleShape(sides[0], sides[1], sides[2]);
		
      System.out.println("\nTriangle Type: " + type.toString());
	}
}