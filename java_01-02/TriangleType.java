import java.io.*;
import java.util.*;

public class TriangleType {
	
	public enum Type{
		SCALENE, ISOCELES, EQUILATERAL, ERROR;	
		public String toString() {
			//only capitalise the first letter
			String s = super.toString();
			return s.substring(0, 1) + s.substring(1).toLowerCase();
		}
	}
	
	static Type getTriangleType(int x, int y, int z) {
		Type type = null;
		
		if(x <= 0 || y <= 0 || z <= 0){
			type = Type.ERROR;
		} else if(x!=y && y!=z && x!=z) {
			type = Type.SCALENE;
		} else if(x==y && y==z && z==x) {
			type = Type.EQUILATERAL;
		} else if(x==y || y==z || x == z) {
			type = Type.ISOCELES;
		}		
		
		return type;
	}
      
   public static void main(String[] args)throws IOException {
   
		Scanner key=new Scanner(System.in);

		System.out.println("Enter the length of each side of the triangle");

		int i = 1;
		int[] side = new int[3];
		while(i<4) {
			System.out.print("Side " + i + ":\t");
				side[i-1] = key.nextInt();							
			i++;
		}
		
		Type type = getTriangleType(side[0], side[1], side[2]);
		
		System.out.println("Triangle Type:\t" + type.toString());

	}

}