import java.util.*;

public class TriangleTypes{
   
   public static void main (String[]args){
   
      Scanner input=new Scanner(System.in);
      
      System.out.println("Enter length of side 1: ");
      int side1=input.nextInt();
      System.out.println("Enter length of side 2: ");
      int side2=input.nextInt();
      System.out.println("Enter length of side 3: ");
      int side3=input.nextInt();
      
      Triangle triangle=new Triangle(side1, side2, side3);
       
      triangle.triangleShape(); 
   }
}