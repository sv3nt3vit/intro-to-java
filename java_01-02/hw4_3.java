/***************************
 Program's 4_2 task: create
 class Dequeue and
 demonstrate it in main method
 Programmer's Name: Ihar Laziuk
 Last updated: 22.10.2014
 **************************/
class Stack {
   public static void main(String[] args)
      {
      StackX theStack = new StackX(10);  // make new stack

      for(int j=0; !theStack.isFull(); j++) // push 10 items
         theStack.push(j*10);            //    (deque wraps)

      while( !theStack.isEmpty() )       // until it's empty,
         {                               // pop items
         long value = theStack.pop();
         System.out.print(value + " ");  // display items
         }                               // 90, 80, ..., 10, 0
      System.out.println("");
   }  // end main()
}

class StackX
   {
   private int maxSize;
   private long[] queArray;
   private int front;
   private int rear;
   private int nItems;
//--------------------------------------------------------------
   public StackX(int s)          // constructor
      {
      maxSize = s;
      queArray = new long[maxSize];
      front = 0;
      rear = maxSize-1;
      nItems = 0;
      }
//--------------------------------------------------------------
   public void insert(long j)   // put item at rear of queue
      {
      if(rear == maxSize-1)         // deal with wraparound
         rear = -1;
      queArray[++rear] = j;         // increment rear and insert
      nItems++;                     // one more item
      }
//--------------------------------------------------------------
   public long remove()         // take item from front of queue
      {
      long temp = queArray[front++]; // get value and incr front
      if(front == maxSize)           // deal with wraparound
         front = 0;
      nItems--;                      // one less item
      return temp;
      }
//--------------------------------------------------------------
   public long peekFront()      // peek at front of queue
      {
      return queArray[front];
      }
//--------------------------------------------------------------
   public boolean isEmpty()    // true if queue is empty
      {
      return (nItems==0);
      }
//--------------------------------------------------------------
   public boolean isFull()     // true if queue is full
      {
      return (nItems==maxSize);
      }
//--------------------------------------------------------------
   public int size()           // number of items in queue
      {
      return nItems;
      }
           
   //begin of my code  
   public void push(long j) {
   long value = j;
   		insertLeft(value);
   	}
   public long pop(){
   	
   		return (removeLeft());
   	}

   public void insertLeft(long j)   
      {
      if(front == maxSize)      
         front = 0;
      queArray[front++] = j;        
      nItems++;                     
      }
   public void insertRight(long j)   
      {
      if(rear == maxSize-1)      
         rear = 0;
      queArray[--rear] = j;        
      nItems++;                     
      }   

   public long removeRight()         
      {
      long temp = queArray[front++]; 
      if(front == maxSize)           
         front = 0;
      nItems--;                     
      return temp;
      }
   public long removeLeft()        
      {
      long temp = queArray[rear--]; 
      if(rear == -1)           
         rear = maxSize-1;
      nItems--;                      
      return temp;
   }// end of my code
}  // end class StackX
