package tutorials;

import javax.swing.*;
import javax.swing.border.Border;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class LoginFrame extends JFrame {
	
//======== create fields (global variables)=========
   private JPanel panel;
   
   private JTextField userNameTextField;
   private JTextField pswTextField;
   
   private JButton loginButton;
   private JButton cancelButton;
   private JButton forgotPswButton;
   
   private String storedUserName = "csc210";
   private static String storedPsw;
   private int cnt=0;
   
   private static String tempVar;
   private static StringBuilder newPsw;
   
   
 //create and assemble the frame
   public LoginFrame() {
      setTitle("Log in");
      setSize(350, 160);	//fixed size of Log in window
      
      createComponents();
      setVisible(true);
      setDefaultCloseOperation(EXIT_ON_CLOSE);   
   }
   
   
   // create individual components
   //JPanel, JLabel, JButton and add them to
   //the instance panel and to the frame
   public void createComponents() {
    
      panel=new JPanel();
      
      //create Bordered Title
      Border titled = BorderFactory.createTitledBorder("Log in");
      panel.setBorder(titled);
      panel.setLayout(new BorderLayout());
      
      JLabel userNameLabel=new JLabel("User Name ");
            
      //create User Name textField, register to ActionListener
      userNameTextField=new JTextField(12);  // size of text area     
      userNameTextField.addActionListener(new TextListener());
            
      JLabel pswLabel=new JLabel("Password ");
      
      //create Password textField, register to ActionListener
      pswTextField = new JPasswordField(12);
      ((JPasswordField) pswTextField).setEchoChar('*');	//set Echo char * to hide psw
      pswTextField.addActionListener(new TextListener());
      
      
      JPanel buttonPanel = new JPanel(); 	//panel to attach buttons
      
    //create Log in button, register to ButtonActionListener
      loginButton = new JButton("Log in");
      loginButton.addActionListener(new LoginButtonListener());
      loginButton.setMnemonic(KeyEvent.VK_L);
      buttonPanel.add(loginButton);	//add to buttonPanel
      
      //create Cancel button, register to ButtonActionListener
      cancelButton = new JButton("Cancel");
      cancelButton.addActionListener(new CancelButtonListener());
      cancelButton.setMnemonic(KeyEvent.VK_C);
      buttonPanel.add(cancelButton); //add to buttonPanel
      
    //create Forgot Psw button, register to ButtonActionListener
      forgotPswButton = new JButton("Forgot password");
      forgotPswButton.addActionListener(new ForgotPswButtonListener());
      forgotPswButton.setMnemonic(KeyEvent.VK_F);
      buttonPanel.add(forgotPswButton); //add to buttonPanel
            
      //add to panel fields of user name
      JPanel userNamePanel=new JPanel(); 
      userNamePanel.add(userNameLabel);
      userNamePanel.add(userNameTextField);

      //add to panel fields of psw
      JPanel pswPanel=new JPanel();     
      pswPanel.add(pswLabel);
      pswPanel.add(pswTextField);
             
      //add all components to the panel and set Layout
      panel.add(userNamePanel, BorderLayout.NORTH);
      panel.add(pswPanel,BorderLayout.CENTER); 
      panel.add(buttonPanel, BorderLayout.SOUTH);
                              
      add(panel);
      
      //pack();  //can be used instead of fixed size   
   }//end CreateComponents() method
   
   
   private class LoginButtonListener implements ActionListener  {
	 //override actionPerformed method
	   public void actionPerformed (ActionEvent event) {
		  
		if (cnt == 0)	//haven't tried to enter psw yet
			storedPsw = "javajava";	//default psw
		else 
			storedPsw = getNewPsw();	//psw no more longer default
		
		//	check for length of entered values, if out of allowed range, do not proceed further
			if (userNameTextField.getText().length() < 20 && pswTextField.getText().length() < 12){
				
				//if entered credentials match records Log in hide this window and proceed to the next 
			   if (userNameTextField.getText().equals(storedUserName) && pswTextField.getText().equals(storedPsw) && cnt <= 3){
				   setVisible(false);
				   MenuFrame menuFrame = new MenuFrame();			   
				   menuFrame.setVisible(true);
			   }
			   
			   //entered credentials do not match records
			   else{
				   if (cnt > 3){	//exit when reached max number of attempts
						JOptionPane.showMessageDialog(null, "You have used up all attempts ... Closing");
						System.exit(0);
					}
				   
				   JOptionPane.showMessageDialog(null, "Incorrect username and/or password, try again!");
				   pswTextField.setText("");
				   cnt++;	//track number of attempts to Log in		   
			   } 
			} 
			
			else{
				JOptionPane.showMessageDialog(null, "Entered values exceed allowed length");
				userNameTextField.setText("");
				pswTextField.setText("");
			} // end of check for the length
			
	   }//end actionPerformed() method   
  } //end class LoginButtonListener  
   
   
   private class CancelButtonListener implements ActionListener  {
	 //override actionPerformed method for Cancel button pressed
	   public void actionPerformed (ActionEvent event){
		
		   userNameTextField.setText("");
		   pswTextField.setText("");		   
	   }
   }
   
   
   //Forgot password button clicked, generate new psw here
   private class ForgotPswButtonListener implements ActionListener  {
	 
	   //override actionPerformed method for Forgot Psw button pressed
	   public void actionPerformed (ActionEvent event){
		   //event.getSource();
		   
		String str="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-*/?!@#$%&";
			
		newPsw = new StringBuilder(); //var holds new psw value
		    
		Random rand=new Random();	//class to generate random chars
		// formula generating a number within a range rand.nextInt((max-min)+1)+min)
		
		newPsw.append(str.charAt(rand.nextInt(25)));	//generate an upper case char
		newPsw.append(str.charAt(rand.nextInt((51-26)+1)+26));	//generate a lower case char
		newPsw.append(str.charAt(rand.nextInt((61-52)+1)+52));	//generate a digit
		newPsw.append(str.charAt(rand.nextInt((str.length()-1-61)+1)+61));	//generate a special symbol
	          
	    // randomly generate the rest of characters (4) and append to the password
		for (int i = 0; i < 4; i++)
			newPsw.append(str.charAt(rand.nextInt(str.length())));
				 
			//convert newPsw (a StringBuilder object) to a String for further operations
			tempVar =newPsw.toString();
 
			//call the shuffle() method to randomize the generated password. Pass a String argument
			shuffle(tempVar);
		
			cnt++;	
	   }//end actionPerformed local method
   }//end ForgotPswButtonListener class
   
   
   //inner class : a class inside another class
      
   private class TextListener implements ActionListener {	   
	 //override actionPerformed method 
	   public void actionPerformed(ActionEvent event) {  
		   
		   //cursor is the User Name field 
		   if(event.getSource() == userNameTextField){
    		  String userName = userNameTextField.getText(); //this var is not used
    	  }
    		  
		 //cursor in Password text field
		   if (event.getSource() == pswTextField){
    		  String password = pswTextField.getText();	//this var is not used
    	  }
         
      } //end actionPerformed local method
   }//end TextListener class
   
   
   
   public void keyReleased(KeyEvent event){
	   //pswTextField.addActionListener(new ActionListener());
	   int key = event.getKeyCode();
	   if(event.getSource() == pswTextField){
		   if(key == KeyEvent.VK_ENTER){
			   setVisible(false);
			   MenuFrame menuFrame = new MenuFrame();			   
			   menuFrame.setVisible(true);
		   }
	   }
   }
   
   
   
   //	MAIN method creates an instance of LoginFrame to run the code
   public static void main(String[] args){
      
	   LoginFrame frame=new LoginFrame(); 
	   //MenuFrame menuFrame = new MenuFrame();  
   
   }//end main method
   
   
   /**
	 *  This shuffle() method is used to randomize generated password. 
	 *  @param tempVar this is a String var that is passed from the main method
	 *  @param ch This char variable ch is used in a for loop to convert String to an arr of chars
	 *  @param shuffled is an array of chars to hold the converted value from String tempVar
	 *  @no return in this method. Because the method itself has the print.out statement
	 */
			
	public static void shuffle (String str){
		
		newPsw.setLength(0);	//clear a StrBld object created before to reuse
		tempVar=str;
		
		//convert tempVar to an arrayList of characters
		ArrayList<Character> psw = new ArrayList<>(tempVar.length());
		 
		 for (char ch : tempVar.toCharArray())
			 psw.add(ch);
		 
		 /*shuffle psw's contents. Because first 4 letters of the psw
		 were generated, however, randomly, but not in a random order:
		 UpperCase - LowerCase - Digit - SpecialSymbol*/
		 
		 Collections.shuffle(psw);

		 for(int i=0; i<psw.size(); i++){
			 newPsw.append(psw.get(i));
		 }
		 
		 storedPsw = newPsw.toString(); //assign newly generated value to a var storedPsw
		 
		 //	display new password
		 JOptionPane.showMessageDialog(null,"Your temporary password is => "+storedPsw);
		 System.out.println("Your temporary password is => " + storedPsw);
		 
	}//end shuffle() method
	
	//this method is used solely to transmit value of newPsw to encrypt method
	public static String getNewPsw (){
		String psw = storedPsw;
		 return psw;
	 }	
	
}//end LoginFrame public class
 
   
   

