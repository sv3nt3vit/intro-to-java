/**
Program Name: MenuFrame
Purpose: see in About menuItem
UserName = csc210
Default Password = javajava
Developer: Ihar Laziuk
Last Updated: 2014.05.16
*/
package tutorials;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

public class MenuFrame extends JFrame {
   
   //======== create fields (global variables)=========
   private JMenuBar menuBar;
   
   private JMenuItem openItem;
   private JMenuItem saveAsItem;
   private JMenuItem closeItem;
   private JMenuItem exitItem;
   
   private JMenuItem encryptItem;
   private JMenuItem decryptItem;
   
   private JMenuItem aboutProgramItem;
   private JMenuItem aboutDeveloperItem;
   
   private JPanel panel;
    
   private JTextArea area;
   private JScrollPane scrollPane;
   
   private Font font = new Font("Monospaced", Font.PLAIN, 12);
   private static boolean endOfFile=false;
   private static String text;
      
   
   //create and assemble the frame
   public MenuFrame(){
      setTitle("CSC210 Final");
      //setSize(300, 300);
      
      menuBar = new JMenuBar();
      setJMenuBar(menuBar);
         
      createComponents();
      pack();
     
      setVisible(true);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
   }
   
   //call methods to create individual components
   public void createComponents(){
	   
      buildFileMenu();
      buildSecurityMenu();
      buildAboutMenu();
      createTextArea();
      createPanel();
   
   }
   
   
   //create component TextArea and set its properties
   public void createTextArea(){
	   
	   area = new JTextArea( 15 , 25 );
	   area.setMargin(new Insets(5,5,5,5));
	   area.setLineWrap(true);
	   area.setWrapStyleWord(true);
  	   area.setEditable(true);
	   scrollPane = new JScrollPane(area);	
	   
   }
   
   
   //create component Panel 
   public void createPanel(){
	   
	   panel = new JPanel();                     
	   panel.add(scrollPane, BorderLayout.CENTER);            
	   add(panel);
	   
   }
   
   
   //create and assemble File Menu
   //add ActionListener to all Menu Items
   //and set all menu items mnemonic for navigation
   public void buildFileMenu(){
   
      MenuListener listener = new MenuListener();
      
      JMenu fileMenu = new JMenu("File"); 
      fileMenu.setMnemonic(KeyEvent.VK_F);
      
      openItem = new JMenuItem("Open");
      openItem.setMnemonic(KeyEvent.VK_O);
      openItem.addActionListener(listener);
      
      saveAsItem = new JMenuItem("Save As");
      saveAsItem.setMnemonic(KeyEvent.VK_S);
      saveAsItem.addActionListener(listener);
      
      closeItem = new JMenuItem("Close");
      closeItem.setMnemonic(KeyEvent.VK_C);
      closeItem.addActionListener(listener);
      
      exitItem = new JMenuItem("Exit");
      exitItem.setMnemonic(KeyEvent.VK_E);
      exitItem.addActionListener(listener);
         
      fileMenu.add(openItem);
      fileMenu.addSeparator();   //add a line separator
      fileMenu.add(saveAsItem);
      fileMenu.addSeparator();   //add a line separator
      fileMenu.add(closeItem);
      fileMenu.addSeparator();   //add a line separator
      fileMenu.add(exitItem);
          
      menuBar.add(fileMenu);	//add to MenuBar
    
   }
   
   
   //create and assemble Security Menu
   //add ActionListner to all menu items
   //and set all menu items mnemonic for navigation
   public void buildSecurityMenu(){
	   
	      MenuListener listener = new MenuListener();
	      
	      JMenu securityMenu = new JMenu("Security"); 
	      securityMenu.setMnemonic(KeyEvent.VK_S);
	      
	      encryptItem = new JMenuItem("Encrypt");
	      encryptItem.setMnemonic(KeyEvent.VK_E);
	      encryptItem.addActionListener(listener);
	      
	      decryptItem = new JMenuItem("Decrypt");
	      decryptItem.setMnemonic(KeyEvent.VK_D);
	      decryptItem.addActionListener(listener);    
	      
	      securityMenu.add(encryptItem);
	      securityMenu.addSeparator();   //add a line separator
	      securityMenu.add(decryptItem);
	          
	      menuBar.add(securityMenu);	//add to MenuBar
	    
	   }
   
   

   //create and assemble About Menu
   //add ActionListner to all menu items
   //and set all menu items mnemonic for navigation
   public void buildAboutMenu(){
	   
	      MenuListener listener = new MenuListener();
	      
	      JMenu aboutMenu = new JMenu("About"); 
	      aboutMenu.setMnemonic(KeyEvent.VK_A);
	      
	      aboutProgramItem = new JMenuItem("Program");
	      aboutProgramItem.setMnemonic(KeyEvent.VK_P);
	      aboutProgramItem.addActionListener(listener);
	      
	      aboutDeveloperItem = new JMenuItem("Developer");
	      aboutDeveloperItem.setMnemonic(KeyEvent.VK_D);
	      aboutDeveloperItem.addActionListener(listener);   
	      
	      aboutMenu.add(aboutProgramItem);
	      aboutMenu.addSeparator();   //add a line separator
	      aboutMenu.add(aboutDeveloperItem);
	          
	      menuBar.add(aboutMenu);	//add to MenuBar
	    
	   }
   
   
   private class MenuListener implements ActionListener{
      //override actionPerformed method
      public void actionPerformed (ActionEvent event) {
         
          //	respond if Open menu item clicked
    	  if(event.getSource()==openItem){
                      
        	  JFileChooser chooser = new JFileChooser();	//class opens Open file window
        	  int status = chooser.showOpenDialog(null); // will pop up the save as 
        	  area.setText("");	//clear TextArea
            
        	//   if Open clicked proceed to save the file
            if(status == JFileChooser.APPROVE_OPTION){
                
            	//open a file object, display file name, path, and set window title=name of file
            	try{
            	  File file = chooser.getSelectedFile();
                  //area.setText("Opening file: " + file.getName() + ".\n"); //display file name
               	  
                  Scanner chosenFile = new Scanner(file);
                  
                  //setTitle(file.getName());	//set title of the window = name if file
                  //area.append(file.getPath()+"\n");	//display path
                 
                  //read the contents from the chosen file
                  while(chosenFile.hasNext()){
	            	  area.append(chosenFile.nextLine()+" ");               		  
	              }
	              
	              chosenFile.close();	//close scanner stream
                  
	              //catch exception if file not found or else
            	} catch (FileNotFoundException e) {
            		area.setText("No valid file found");
            	}
            }
            
            
            //if clicked Cancel 
            if(status == JFileChooser.CANCEL_OPTION){
            	 area.setText("You pressed cancel");         	 
             }
                                                 
          }	//end if event==OpenItem
          
          
    	  //    respond if Save menu item clicked
    	  if(event.getSource() == saveAsItem){
              
              JFileChooser chooser = new JFileChooser();	//class opens save file window
              int status = chooser.showSaveDialog(null); // will pop up the save as 
                 
              //if Save clicked
              if(status == JFileChooser.APPROVE_OPTION){
            	try{
            	  File file = chooser.getSelectedFile();                             
                  
                  setTitle(file.getName());
                  //area.append(file.getPath()+"\n");
                  
                  PrintWriter writeToFile = new PrintWriter(file);// object to write to a file
                  area.write(writeToFile); 
                  
                  writeToFile.close();	//close Scanner stream
                  area.setText("");	//clear TextArea
                  
            	} catch (IOException e){
            		area.append("File with this name not found");
            	}           	
              }
              
              
              //if Cancel clicked 
              if(status == JFileChooser.CANCEL_OPTION){
              	 area.setText("You pressed cancel");         	 
               }
              
           }
          
          
          if (event.getSource()==closeItem) {
            //JOptionPane.showMessageDialog(null, "Close a file");  
            setTitle("");  //remove the title from TitleBar  
            //next should clear the area   
            area.setText("");	//clear TextArea
          }
          
          
          //exit from the program if Exit clicked
          if (event.getSource()==exitItem) {
             System.exit(0);       
          }
          
          
          // if Encrypt clicked encrypt displayed text
          if (event.getSource() == encryptItem){
        	  setTitle("Encrypt");
        	  
        	  JFileChooser chooser = new JFileChooser();
              int status = chooser.showSaveDialog(null); // will pop up the save as    
              
           //Enter or Save clicked - encrypt selected
           //text and save to a binary file  
           if(status == JFileChooser.APPROVE_OPTION){
        	try{
        		File file = chooser.getSelectedFile();     
	      		String message = area.getText();
	      
	      		message = encrypt(message);	//call encrypt() method to encrypt displayed text	      		      		                        
                
                setTitle(file.getName());
	      		
	      		//open created .dat binary file for input
	      		DataOutputStream outputFile = new DataOutputStream(new FileOutputStream(file));			
	      		
                    //write to a file                     
        			try{
        				outputFile.writeUTF(message);
        			} catch (EOFException e){
        				JOptionPane.showMessageDialog(null, "Couldn't write to a file");
        			}
                
      			outputFile.close();	//close Scanner stream
                
                area.setText("");	//clear TextArea
                
	        } catch (IOException e){
	        	JOptionPane.showMessageDialog(null, "Oops, something went wrong while encrypting");
	        }
        }
           
         //if Cancel clicked 
           if(status == JFileChooser.CANCEL_OPTION){
           	 area.setText("You pressed cancel");         	 
            }
     }     
        	  
          
          //if Decrypt clicked, open a file and 
          //decrypt its contents, then display in TextArea
          if (event.getSource() == decryptItem){
        	  setTitle("Decrypt");
        	  
        	  JFileChooser chooser = new JFileChooser();
              int status = chooser.showOpenDialog(null); // will pop up the save as 
              
           //if Decrypt selected proceed   
           if(status == JFileChooser.APPROVE_OPTION){
        	try{
        		File file = chooser.getSelectedFile();        		      		                        
                
                setTitle(file.getName());
	      		
	      		//open created .dat binary file for input
	      		DataInputStream inputFile = new DataInputStream(new FileInputStream(file));	
	      		
	      		//read the file until its end
	      		while(!endOfFile){
	      			try{
	      				text = inputFile.readUTF();
	      			} catch (EOFException e){
	      				endOfFile = true;
	      			}
	      		}
	      		
	      		text = decrypt(text);	//call decrypt() method to decrypt saved text	  
                                         
	      		area.append(text);	//desplay decrypted text in TextArea
	      		      			            
        		inputFile.close();	//close Scanner stream
                
        	//	throw an Exception is case something goes wrong	
        	} catch (IOException e){
	        	JOptionPane.showMessageDialog(null, "Oops, something went wrong while decrypting");
	        }
        }
           
         //if Cancel clicked return back 
           if(status == JFileChooser.CANCEL_OPTION){
           	 area.setText("You pressed cancel");         	 
            }
     }  
                  	  
        	  
        	  
          //About Program menu item is selected
          //display info about the program
          if (event.getSource() == aboutProgramItem){        	  
        	  setTitle("About Program");
        	  area.setText("This Program prompts user to enter User Name and password "+
        			  		"and validates both the User Name and the password. If "+
        			  		"user forgot password, program gives an option to reset it. "+
        			  		"After user has successfully logged in, a main window frame "+
        			  		"appears on the screen with three menu Items: File, Security "+
        			  		"and About. Each Menu Item has several options to choose from: "+
        			  		"open a file, save a file, close, exit, encrypt ot decrypt a file. "+
        			  		"'About' menu contains information about the program and developer.");
        	  area.setEditable(false);	//prevent from overriding
        	  area.setFont(font);	//set monospace font
          }
          
          
          //About Developer menu item is selected
          //display developer's contact information
          if (event.getSource() == aboutDeveloperItem){
        	  setTitle("About Developer");
        	  area.setText("Developer: Ihar Laziuk\n\n"+
        			  		"Contact information:\n\n"+
        			  		"\ttel# 347.5544.727\n"+
        			  		"\temail: skjorgg@gmail.com\n"+
        			  		"\tihar.laziuk@zoho.com\n"+
        			  		"\tResidency place: New York, NY\n"+
        			  		"\tInstituition: Columbia University");
        	  area.setEditable(false);	//prevent from overriding
        	  area.setFont(font);	//set monospace font
          }        
      }
   }
   
   
	/**
 	 * This method encrypt() takes a 
 	 * String arg, encrypts it and
 	 * saves to a .dat binary file. 
 	 * @param message
 	 * @throws IOException
 	 * @param no return
 	 */
 	public String encrypt (String message)throws IOException{
 		
 		char[] convertedMessage=message.toCharArray();	//convert String message to an array of chars
 		char[] encrypted = new char [convertedMessage.length];
		
 		//write the numbers to a file and add 10 to each character, so the new
 		//character will have a different value in ASCII code (shift it by 10).
 		for(int i=0; i<convertedMessage.length; i++){
 			encrypted [i] = (char) (convertedMessage[i]+10);
 		}
 		
 		String convert = new String(encrypted);
 		
 		return convert;
 	}
 	
 	
 	/**
 	 Program Name: DecryptionFilter
 	 Purpose:
 	 		This program decrypts the file
 	 		created in Encryprion;
 	 		restores the data, writes to a new file.
 	 Programmer: Ihar Laziuk
 	 Last Updated: 2014.05.06 
 	 */
	public String decrypt (String message)throws IOException{
 		
		char[] encryptedText=message.toCharArray();	//convert String message to an array of chars
 		char[] decrypted = new char [encryptedText.length];
		
 		//write the numbers to a file and add 10 to each character, so the new
 		//character will have a different value in ASCII code (shift it by 10).
 		for(int i=0; i<encryptedText.length; i++){
 			decrypted [i] = (char) (encryptedText[i]-10);
 		}
 		
 		String convert = new String(decrypted);
 		
 		return convert;
 	}
 	
   
   //MAIN method creates an instance of MenuFrame and runs GUI
   public static void main (String[] args) {
   
      MenuFrame menuFrame = new MenuFrame();
   }
   
}