/**
 Program Name: DecryptionFilter
 Purpose:
 		This program decrypts the file
 		created in EncryprionFilter class;
 		restores the data, writes to a new file.
 Programmer: Ihar Laziuk
 Last Updated: 2014.05.06 
 */
package Assignment7;
import java.util.*;
import java.io.*;

public class DecryptionFilter {
	
	private static DataInputStream inputFile1;
	private static DataInputStream inputFile2;
	private static DataOutputStream outputFile1;
	private static char [] convertedMessage;
	private static boolean endOfFile=false;	//EOF Flag
	
	public static void main(String[] args) throws IOException {
		
		convertedMessage=new char[100];
		
		char ch;
		
		//	open a file for reading and writing
		inputFile1 = new DataInputStream (new FileInputStream("SecretMessage.dat"));		
		outputFile1 = new DataOutputStream (new FileOutputStream("DecryptedSecretMessage.dat"));
		inputFile2 = new DataInputStream (new FileInputStream("DecryptedSecretMessage.dat"));
		
		while(!endOfFile){
			try{
				for(int i=0; i<convertedMessage.length; i++){
					convertedMessage [i] =inputFile1.readChar();
					outputFile1.writeChar(convertedMessage [i]-10);
				}
			} catch (EOFException e){
				endOfFile=true;
			}
		}
		
		displayDecryptedMessage();
				
		inputFile1.close();
		outputFile1.close();				
	}
	 public static void displayDecryptedMessage() throws IOException{
		 	 
		 while(!endOfFile){
			 try{
				 for(int i=0; i<convertedMessage.length; i++){
			        convertedMessage [i] =inputFile2.readChar(); 
					System.out.print(convertedMessage[i]);
				 }
			} catch (EOFException e){
				endOfFile=true;
			}
		}
		 
	 }
	
}
