/**
 Program Name: DeserializedTestScores
 Purpose:
 		This program deserializes The TestScores
 		class objects in the Objects.dat file and
 		stores data in the array.
 Programmer: Ihar Laziuk
 Last Updated: 2014.05.06 
 */
package Assignment7;
import java.util.*;
import java.io.*;

public class DeserializedTestScores {

	public static void main(String[] args) throws Exception {
		
		double [] scores=new double[10];	//an array to hold scores for each test
		final int NUM_OF_ACCOUNTS=5;	//set num of accounts
		
		//create the stream objects
		ObjectInputStream objectInputFile=new ObjectInputStream (new FileInputStream("Objects.dat"));
		
		//create a TestScores array to hold Test array objects
		TestScores [] testScores=new TestScores[NUM_OF_ACCOUNTS];
		
		//read the serialized objects from the file
		for (int i=0; i<testScores.length; i++){
			for (int j=0; j<scores.length; j++){
				testScores[i]=(TestScores) objectInputFile.readObject();
			}
		}
		
		objectInputFile.close();
		
		//display the objects
		for (int i=0; i<testScores.length; i++){
			for (int j=0; j<scores.length; j++){
				System.out.println("Test "+(i+1)+testScores[j].getAverage());
			}
		}

	}

}
