/**
 Program Name: EncryptionFilter
 Purpose:
 		This program reads in a message from
 		a user and modifies it applying a key
 		for encryption. Next it opens a binary
 		file and writes content. Next displays it.
 Programmer: Ihar Laziuk
 Last Updated: 2014.05.06 
 */
package Assignment7;
import java.util.*;
import java.io.*;

public class EncryptionFilter {
		
	public static void main(String[] args)throws IOException {
		
		Scanner key=new Scanner(System.in);
		String message;
		boolean endOfFile=false;	//EOF Flag
		char ch;
		
		System.out.println("Enter your message: ");
		message=key.nextLine();
		
		encrypt(message);	//call encrypt() method to modify user's message
		
		//open created earlier and modified .dat binary file for input
		DataInputStream inputFile = new DataInputStream(new FileInputStream("SecretMessage.dat"));		
		
		System.out.print("\nYour encrypted message: ");
		
		//display encrypted message, use boolean endOfFile to exit the while loop
		while(!endOfFile){
			try{
				ch=inputFile.readChar();
				System.out.print(ch);
			
			} catch (EOFException e){
				endOfFile=true;
			}
		}
		
		inputFile.close();	//close input stream object
		key.close(); 	//close Scanner object
	}
	
	/**
	 * This method encrypt() takes a 
	 * String arg, encrypts it and
	 * saves to a .dat binary file. 
	 * @param message
	 * @throws IOException
	 * @param no return
	 */
	public static void encrypt(String message)throws IOException{
		
		char[]convertedMessage=message.toCharArray();	//convert String message to an array of chars
		
		//open a binary file and wrap in DataOutputStream object
		DataOutputStream outputFile=new DataOutputStream(new FileOutputStream("SecretMessage.dat"));
		
		//write the numbers to a file and add 10 to each character, so the new
		//character will have a different value in ASCII code (shift it by 10).
		for(int i=0; i<convertedMessage.length; i++)
			outputFile.writeChar(convertedMessage[i]+10);
		
		outputFile.close(); 	//close the file
	}
}
