/**
 Program Name: ExceptionsInPayrollExample
 Purpose:
	   		This program demonstrates how
	   		the Payroll class constructor
	   		throws custom exceptions.
Programmer: Ihar Laziuk
Last updated: 2014.05.06
*/
package Assignment7;
import java.util.*;
import java.io.*;

public class ExceptionsInPayrollExample {
	public static void main(String [] args){
		
		Payroll payroll=new Payroll("Cheking", 102030);	//create an instance of a payroll first
		
		//force a NonValidNameException
		try{
			payroll.setName("");
			
		} catch (NonValidNameException e) {
			System.out.println(e.getMessage());
		}
		
	}

}
