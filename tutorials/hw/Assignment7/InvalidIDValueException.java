
/**
The InvalidIDValueException is thrown by the
Payroll class when an invalid value is passed
for the employee's ID number in the constructor.
*/
public class InvalidIDValueException extends Exception {
	/**
	 This constructor specifies the bad ID number
	 trying to pass in the error message
	 @param Invalid Employee's ID number
	 */
	
	public InvalidIDValueException(int idNumber) {
		super("Error: Invalid employee's ID number: "+idNumber);
	}
}
