
The InvalidIDValueException is thrown by the
Payroll class when an invalid number is passed
for the number of hours worked e.g. a negative
number or a number>84 hours.
*/

public class InvalidNumberOfHoursWorkedException extends Exception{
	
	/**
	 This constructor specifies the invalid
	 number of hours worked in the error message
	 @param Invalid number of hours worked.
	 */
	
	public InvalidNumberOfHoursWorkedException(double hoursWorked) {
		super("Error: Invalid number of hours worked: "+hoursWorked);
	}
}

