
/**
The InvalidPayRateException is thrown by the
Payroll class when an invalid number is passed
for the hourly pay rate; e.g. a negative
number or a number>25.
*/
public class InvalidPayRateException extends Exception{
	/**
	 This constructor specifies the invalid 
	 hourly pay rate in the error message
	 @param Invalid pay rate.
	 */
	public InvalidPayRateException(double payRate) {
		super("Error: Invalid pay rate: "+payRate);
	}
}
