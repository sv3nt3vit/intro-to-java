package Assignment7;
/**
The NonValidNameException is thrown by the
Payroll class when an empty string for the
name parameter is passed to the constructor.
*/
public class NonValidNameException extends Exception{

	/**
	 This constructor uses a generic
	 error message
	 */
	public NonValidNameException() {
		super("Error: Empty string for the name passed");
	}
}
