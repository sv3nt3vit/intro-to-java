/**
 Program Name: ExceptionsInPayrollExample
 Purpose:
	   		This program demonstrates how
	   		the Payroll class constructor
	   		throws custom exceptions.
Programmer: Ihar Laziuk
Last updated: 2014.05.06
*/
package Assignment7;

/**
The Payroll class stores data about an employee's pay
for the Payroll Class programming challenge.
*/

public class Payroll {
	
	private String name;          // Employee name
	private int idNumber;         // ID number
	private double payRate;       // Hourly pay rate
	private double hoursWorked;   // Number of hours worked

	/**
	   The constructor initializes an object with the
	   employee's name and ID number.
	   @param n The employee's name.
	   @param i The employee's ID number.
	*/

	public Payroll(String n, int i){
	   name = n;
	   idNumber = i;
	}
	
	/**
	   The setName sets the employee's name.
	   @param n The employee's name.
	   @exception NonValidNameException when
	   given an invalid name e.g. an empty string
	*/
	
	public void setName (String n) throws NonValidNameException{
	   if (n==null)
		   throw new NonValidNameException(); 
	   
		name = n;
	}
	
	
	/**
	   The setIdNumber sets the employee's ID number.
	   @param i The employee's ID number.
	   @exception InvalidIDValueException when
	   given an invalid employee's number (i) such
	   as a negative number or a zero.
	*/	
	public void setIdNumber(int i) throws InvalidIDValueException{
	    if(i<=0)
	    	throw new InvalidIDValueException(i);
	    
		idNumber = i;
	}
	
	
	/**
	   The setPayRate sets the employee's pay rate.
	   @param p The employee's pay rate.
	   @exception InvalidPayRateException when given
	   pay rate (p) is negative or>25.
	*/	
	public void setPayRate(double p) throws InvalidPayRateException{
		if(p<0 || p>25)
			throw new InvalidPayRateException(p);
		
	   payRate = p;
	}
	
	
	/**
	   The setHoursWorked sets the number of hours worked.
	   @param h The number of hours worked.
	   @exception InvalidNumberOfHoursWorkedException when
	   given hours worked (h) are negative or>84.
	*/	
	public void setHoursWorked (double h) throws InvalidNumberOfHoursWorkedException{
		if(h<0 || h>84.0)
			throw new InvalidNumberOfHoursWorkedException(h);
		
	    hoursWorked = h;
	}
	
	
	/**
	   The getName returns the employee's name.
	   @return The employee's name.
	*/	
	public String getName(){
	   return name;
	}
	
	
	/**
	   The getIdNumber returns the employee's ID number.
	   @return The employee's ID number.
	*/
	public int getIdNumber(){
	   return idNumber;
	}
	
	
	/**
	   The getPayRate returns the employee's pay rate.
	   @return The employee's pay rate.
	*/
	public double getPayRate(){
	   return payRate;
	}
	
	
	/**
	   The getHoursWorked returns the hours worked by the
	   employee.
	   @return The hours worked.
	*/	
	public double getHoursWorked(){
	   return hoursWorked;
	}
	
	
	/**
	   The getGrossPay returns the employee's gross pay.
	   @return The employee's gross pay.
	*/	
	public double getGrossPay(){
	   return hoursWorked * payRate;
	}	
	
}