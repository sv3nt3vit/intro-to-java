/**
 Program Name: SerializedTestScores
 Purpose:
 		This program serializes The TestScores
 		class objects in an array of testScores
 		objects and writes them to the Objects.dat file.
 Programmer: Ihar Laziuk
 Last Updated: 2014.05.06 
 */
package Assignment7;
import java.util.*;
import java.io.*;

public class SerializedTestScores {
	
	public static void main(String[] args) throws IOException {
		
		double [] scores=new double[10];	//an array to hold scores for each test
		final int NUM_OF_ACCOUNTS=5;	//set num of accounts
		
		Scanner key=new Scanner(System.in);
		
		//create a TestScores array to hold Test array objects
		TestScores [] testScores=new TestScores[NUM_OF_ACCOUNTS];
		Random rand=new Random();
		
		//populate the array
		for (int i = 0; i<testScores.length; i++){
			System.out.println("Getting test scores for Test #"+(i+1)+" ...\nCompleted\n");
			  
            for (int j=0; j<scores.length; j++)
            	scores[j]=rand.nextInt(100);				
				
            testScores[i]=new TestScores(scores); 	//create an object in the array			 
		 }
		
		//create the stream objects
		ObjectOutputStream objectOutputFile=new ObjectOutputStream (new FileOutputStream("Objects.dat"));
		
		//Write the serialized objects to the file
		for (int i = 0; i < testScores.length; i++){
			objectOutputFile.writeObject(testScores[i]);
		}
		
		objectOutputFile.close();
		
		System.out.println("\nThe serialized objects were written to the Objects.dat file.");

	}
}

