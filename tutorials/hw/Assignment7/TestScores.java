/**
 Program Name: TestScores
 Purpose:
 		This program serializes The TestScores
 		class (created before) that stores data
 		about test scores from programming challenge.
 Programmer: Ihar Laziuk
 Last Updated: 2014.05.06 
 */
package Assignment7;
import java.util.*;
import java.io.Serializable;

public class TestScores implements Serializable {


private double[] scores; //a var to reference array holding test scores

	/**
	   The constructor initializes an object with
	   an array of scores. If the array contains
	   an invalid value (less than 0 or greater than
	   100) an exception is thrown.
	   @param s The array of test scores.
	   @exception IllegalArgumentException When the
	              argument array contains an invalid
	              value.
	*/
	   
	public TestScores(double[] s) throws IllegalArgumentException {
		
	   scores = new double[s.length];	//create an array to hold passed arg test scores
	   
	   /* Copy the scores passed as an argument into
	   	the new array. Check for illegal values as
	   	they are copied.
	   */
	   for (int i = 0; i < s.length; i++){
		   
	      if (s[i] < 0 || s[i] > 100)
	         throw new IllegalArgumentException("Element: " + i + " Score: " + s[i]);
	      else
	         scores[i] = s[i];
	   }
	}
	
	/**
	   The getAverage method returns the average
	   of the object's test scores.
	   @return The average of the object's test scores.
	*/
	
	public double getAverage(){
		
	   double total = 0.0;  // Accumulator
	   
	   // Accumulate the sum of the scores.
	   for (int i = 0; i < scores.length; i++)
	      total += scores[i];
	   	   
	   return (total / scores.length);	// return the average.
	}
}   
