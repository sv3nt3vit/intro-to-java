/*
 * Program Name: ClockConverter
 * Purpose: reads in 12-hour format
 * 			time and converts it to
 * 			24-hour format;
 * Programmer: Ihar Laziuk
 * Last Updated: 2014.16.04
 */

package assignment_6;
import java.util.*;

public class ClockConverter {

	public static void main(String[] args) {
		
		StringBuilder strbld = new StringBuilder();	//create a strbld object for further operations
		
		//get user's input
		Scanner key=new Scanner(System.in);	
		System.out.println("Enter a 12-hour format time, sample input:\t8  :  30 pm ");
		String entry=key.nextLine();
				
		String [] tokens=entry.split("[ :]+");	//extract each word from user's entry and store in an array of Strings
		
		
		/*outer do-while loop checks for duplicate invalid entry e.g. if hours entered incorrect - try again (inner while), 
		hours correct, but minutes incorrect - retry (2nd inner while), next iteration to check both hours and minutes again, 
		(outer do-while loop) instead of checking just minutes, for user might enter first value incorrect again etc ..*/
		do{
			//check for the hours to be within the range 1-12
			while (Integer.parseInt(tokens[0])<1 || Integer.parseInt(tokens[0])>12){
				System.out.println("Invalid input HOURS, try again, sample input:\t8  :  30 pm ");
				entry=key.nextLine();
					
				tokens=entry.split("[ :]+");	//extract each word from user's entry and store in an array of Strings
			}			
	
			//check for minutes to be within the range 1-59
			while (Integer.parseInt(tokens[1])<0 || Integer.parseInt(tokens[1])>59){
				System.out.println("Invalid input MINUTES, try again, sample input:\t8  :  30 pm ");
				entry=key.nextLine();
				
				tokens=entry.split("[ :]+");	//extract each word from user's entry and store in an array of Strings
			}	
		} while((Integer.parseInt(tokens[0])<1 || Integer.parseInt(tokens[0])>12)||	
				(Integer.parseInt(tokens[1])<0 || Integer.parseInt(tokens[1])>59)); //keeps checking for both valid fields
		
		
		//check if user's entry is PM. Case it is - convert hours to 24-h format
		if(tokens.length==3 && tokens[2].equalsIgnoreCase("PM")){
			int convertedTime=Integer.parseInt(tokens[0])+12;
			strbld.append(convertedTime);
		}
		else	//entry is not PM, append to the strbld object initial value
			strbld.append(tokens[0]);
		
		//append to strbld object delimiter ":" and minutes value
		strbld.append(":");
		strbld.append(tokens[1]);
		
		//display converted time
		System.out.println("\nThe Time is: "+ strbld.toString());
			
		key.close();	//close Scanner object	
	}
}