/*
 * Program Name: PasswordReset
 * Purpose: generates new psw:
 * 			8-12 char long, upper case,
 * 			lower case, digit and special
 *			symbols should be present.
 * Programmer: Ihar Laziuk
 * Last Updated: 2014.16.04
 */

package assignment_6;
import java.util.*;

public class PasswordReset {

	public static void main(String[] args) {
		
		String str="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-*/?!@#$%&";
		String tempVar;
		
		Scanner key=new Scanner(System.in);
		System.out.println("Oops! It seems you've forgotten your password\n"+
							"\t\t\tWould you like to reset it? Y/N");
		char userChoice=Character.toUpperCase(key.next().charAt(0));	//read in user's input
	    StringBuilder newPsw = new StringBuilder(); 
	    
		if (userChoice=='Y'){	//user chose to reset password
			 Random rand=new Random();
			// formula generating a number from a range rand.nextInt((max-min)+1)+min)
			 newPsw.append(str.charAt(rand.nextInt(25)));	//generate an upper case char
			 newPsw.append(str.charAt(rand.nextInt((51-26)+1)+26));	//generate a lower case char
			 newPsw.append(str.charAt(rand.nextInt((61-52)+1)+52));	//generate a digit
			 newPsw.append(str.charAt(rand.nextInt((str.length()-1-61)+1)+61));	//generate a special symbol
			 
			 // randomly generate the rest of characters and append to the password
			 for (int i=0; i<8; i++)
				 newPsw.append(str.charAt(rand.nextInt(str.length())));
			 
			 //convert newPsw (a StringBuilder object) to a String for further operations
			 tempVar =newPsw.toString();
			 
			 //call the shuffle() method to randomize the generated password. Pass a String argument
			 shuffle(tempVar);
			 				 
		} else System.out.println("You chose not to reset your password. Buy");	//user chose not to reset psw
	}
	
	/**
	 *  This shuffle() method is used to randomize generated password. 
	 *  @param tempVar This is a String var that is passed from the main method
	 *  @param ch This char variable ch is used in a for loop to convert String to an arr of chars
	 *  @param shuffled is an array of chars to hold the converted value from String tempVar
	 *  @no return in this method. Because the method itself has the print.out statement
	 */
			
	public static void shuffle (String str){
		
		String tempVar=str;
		
		//convert tempVar to an arrayList of characters
		ArrayList<Character> psw=new ArrayList<>(tempVar.length());
		 
		 for (char ch : tempVar.toCharArray())
			 psw.add(ch);
		 
		 /*shuffle psw's contents. Because first 4 letters of the psw
		 were generated, however, randomly, but not in a random order:
		 UpperCase - LowerCase - Digit - SpecialSymbol*/
		 
		 Collections.shuffle(psw);
		 
		 char []randPsw=new char[psw.size()];	//create an array of Char-s with the size of psw()
		 										//this is done solely to format the output
		 
		 //display randomly generated password and fill the before created array with psw's content
		 System.out.print("\nHere is your temporary password => \t");
		 
		 	for(int i=0; i<randPsw.length; i++){
		 		randPsw[i]=psw.get(i);
		 		System.out.print(randPsw [i]);		
		 }
	}
}
