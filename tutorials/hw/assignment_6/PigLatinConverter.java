/*
 * Program Name: PigLatinConverter
 * Purpose: reads in a sentence and
 * 			converts it to PigLatin
 * Programmer: Ihar Laziuk
 * Last Updated: 2014.16.04
 */
package assignment_6;
import java.util.*;

public class PigLatinConverter {

	public static void main(String[] args) {
		
		//create two StringBuilder objects to store initial and formatted values
		StringBuilder strbld;
		StringBuilder strbld2;
		char temp;	//a variable to store the first letter of each word
		
		//import Scanner object and get user's input
		Scanner key=new Scanner(System.in);
		System.out.println("Plese enter a short sentence:");		
		String entry=key.nextLine();
		
		String [] tokens=entry.split("[ .]+");	//extract each word from user's entry and store in an array of Strings
		
		for (int i=0; i<tokens.length; i++){
			temp=tokens[i].charAt(0);	//store the first letter of each word
			
			strbld = new StringBuilder(tokens[i]);	//copy the contents of the array's element to strbld variable
											
			strbld.deleteCharAt(0);	//delete the first letter in each word
			
			strbld2=new StringBuilder(strbld);	//store in variable strbld2 word without 1st letter
			
			strbld2.append(temp+"AY ");	//append to strbld2 first letter in variable temp and "AY" 
			
			System.out.print(strbld2.toString().toUpperCase());	//print user's entry, converted to "pig Latin"		
		}
	}
}
